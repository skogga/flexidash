var express = require('express');
var router = express.Router();
var mongojs = require('mongojs');
var db = mongojs('XXX', ['YYY']);


// Get all slackMessages
router.get('/slackTemplates', function(req, res, next) {

    db.slackMessages.find(function(err, slackTemplates) {
        if (err) {

            res.send(err);
        }

        res.json(slackTemplates);
    });
});

// Get single slackMessage
router.get('/slackTemplate/:id', function(req, res, next) {
    db.slackTemplates.findOne({ _id: mongojs.ObjectId(req.params.id) }, function(err, slackMessage) {
        if (err) {
            res.send(err);
        }
        res.json(slackTemplate);
    });
});



// Update slackMessage
router.put('/slackTemplate/:id', function(req, res, next) {
    var slackTemplate = req.body;
    var updSlackTemplate = {};


    if (slackTemplate.messageTemplate) {
        updSlackMessage.messageTemplate = slackTemplate.messageTemplate;
    }
    if (slackTemplate.messageType) {
        updSlackTemplate.messageType = slackTemplate.messageType;
    }
    if (!updSlackTemplate) {
        res.status(400);
        res.json({
            "error": "Bad data"
        });
    } else {
        db.slackTemplates.update({ _id: mongojs.ObjectId(req.params.id) }, updSlackTemplate, {}, function(err, slackTemplate) {
            if (err) {
                res.send(err);
            }
            res.json(slackTemplate);
        });
    }
});

module.exports = router;