const express = require('express');
const router = express.Router();
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');

var async = require('async');
/* GET api listing. */



router.get('/jiraPuls', function(req, res, next) {

    var currentOnTheBoard = [{
            projectName: 'MAC',
            id: '45796'
        },
        {
            projectName: 'MPP',
            id: '46038'
        },
        {
            projectName: 'MEM',
            id: '46041'
        },
        {
            projectName: 'MACC',
            id: '46419'
        }
    ];

    var consolidatedReleases = [];
    var resps = 0;

    for (var i = 0; i < currentOnTheBoard.length; i++) {
        jiraReleatedIssues(currentOnTheBoard[i].projectName, currentOnTheBoard[i].id, function(resp) {
            resps += 1;
            consolidatedReleases.push(resp);
            if (i == resps) {
                res.send(consolidatedReleases);
            }
        });
    }
});


var jira = function(apiReqOptions, okCallback) {
    var request = require('request');
    request(apiReqOptions, function(error, response, body) {
        if (error) {
            console.log(error);
        } else {
            return okCallback(null, body);
        }
    });
};



function jiraReleatedIssues(projectName, jiraRelease, callback) {

    var jiraRelatedUrl = "https://jira.scania.com/rest/api/2/search?jql=project=" + projectName + " and fixVersion=" + jiraRelease;
    var jiraIssuesInReleaseOptions = {
        method: 'GET',
        json: true,
        url: jiraRelatedUrl,
        headers: {
            Authorization: 'Basic XXX'
        }
    }

    var request = require('request');

    request(jiraIssuesInReleaseOptions, function(error, response, body) {
        if (error) {
            console.log(error);
        } else {

            var issues = body.issues;
            var projectIssues = {

            };
            if (projectName == "MAC") {
                projectIssues.MACissues = [];
            } else if (projectName == "MPP") {
                projectIssues.MPPissues = [];
            } else if (projectName == "MEM") {
                projectIssues.MEMissues = [];
            } else if (projectName == "MACC") {
                projectIssues.MACCissues = [];
            }

            if (issues) {


                for (var i = 0; i < issues.length; i++) {

                    var thisIssue = {};

                    ////////// Hämta all basinfo och strukturera issues //////////
                    thisIssue = {
                        key: issues[i].key,
                        url: "https://jira.scania.com/browse/" + issues[i].key,
                        title: issues[i].fields.summary,
                        status: issues[i].fields.status.name,
                        statusIcon: issues[i].fields.status.iconUrl,
                        type: issues[i].fields.issuetype.name,
                        typeIcon: issues[i].fields.issuetype.iconUrl,
                        labels: [],
                        components: [],
                        // valueItems: [],
                        priority: issues[i].fields.priority.name,
                        priorityIcon: issues[i].fields.priority.iconUrl,
                        description: issues[i].fields.description,
                        reporter: issues[i].fields.reporter.displayName,
                        additionalAssignees: [],
                        issueLinksInward: [],
                        issueLinksOutward: []
                    };
                    if (issues[i].fields.assignee) {
                        thisIssue.assignee = issues[i].fields.assignee.displayName;
                    } else {
                        thisIssue.assignee = 'Unassigned';
                    }

                    if (issues[i].fields.customfield_13386) {

                        thisIssue.valueItem = issues[i].fields.customfield_13386;
                    }

                    if (issues[i].fields.customfield_10111) {
                        thisIssue.severity = issues[i].fields.customfield_10111;
                    }


                    var tempCreated = issues[i].fields.created.substring(0, issues[i].fields.created.search(/\./));
                    var tempUpdated = issues[i].fields.updated.substring(0, issues[i].fields.updated.search(/\./));

                    thisIssue.created = {
                        date: tempCreated.split("T")[0],
                        time: tempCreated.split("T")[1]
                    };
                    thisIssue.updated = {
                        date: tempUpdated.split("T")[0],
                        time: tempUpdated.split("T")[1]
                    };



                    if (issues[i].fields.description) {
                        var techStart = "<Release Notes Technical Start>";
                        var techEnd = "<Release Notes Technical End>";
                        var description = issues[i].fields.description.replace(/\*/g, '');

                        if (description.search(techStart) != -1 && description.search(techEnd) != -1) {

                            var techStripped = description.substring(description.search(techStart) + techStart.length + 1, description.search(techEnd) - description.search(techStart) + techStart.length + techEnd.length + 1);

                            var end = description.search(techEnd);
                            var start = description.search(techStart);
                            var len = (end - start);
                            var str = description.substr(start + techStart.length, len - techStart.length);

                            thisIssue.releaseNotesTechnical = str;
                        }

                        var commercialStart = "<Release Notes Commercial Start>";
                        var commercialEnd = "<Release Notes Commercial End>";

                        if (description.search(commercialStart) != -1 && description.search(commercialEnd) != -1) {

                            var techStripped = description.substring(description.search(commercialStart) + techStart.length + 1, description.search(commercialEnd) - description.search(commercialStart) + commercialStart.length + commercialEnd.length + 1);

                            var end = description.search(commercialEnd);
                            var start = description.search(commercialStart);
                            var len = (end - start);
                            var str = description.substr(start + commercialStart.length, len - commercialStart.length);

                            thisIssue.releaseNotesCommercial = str;
                        }

                    }
                 

                    ////////// Hämta labels //////////
                    if (issues[i].fields.labels.length > 0) {
                        for (var m = 0; m < issues[i].fields.labels.length; m++) {

                            var label = issues[i].fields.labels[m];
                            thisIssue.labels.push(label);

                        }
                    }

                    // External ref. (service center)
                    if (issues[i].fields.customfield_10106) {
                        for (var m = 0; m < issues[i].fields.customfield_10106.length; m++) {
                            if (issues[i].fields.customfield_10106[m].includes("IM")) {
                                thisIssue.serviceCenterId.push(issues[i].fields.customfield_10106[m]);
                            }

                        }
                    }

                    ////////// Hämta components //////////
                    if (issues[i].fields.components.length > 0) {
                        for (var m = 0; m < issues[i].fields.components.length; m++) {
                            thisIssue.components.push(issues[i].fields.components[m]);

                        }
                    }

                    ////////// Hämta additional assignees //////////
                    if (issues[i].fields.customfield_13295) {
                        for (var x = 0; x < issues[i].fields.customfield_13295.length; x++) {
                            thisIssue.additionalAssignees.push(issues[i].fields.customfield_13295[x].displayName);
                        }
                    }



                    ////////// Hämta länkade issues //////////
                    var issueLinks = issues[i].fields.issuelinks;
                    if (issueLinks) {

                        for (var j = 0; j < issueLinks.length; j++) {

                            ////////// Hämta inward(?) issues //////////
                            if (issueLinks[j].hasOwnProperty("inwardIssue")) {
                                var inward = {
                                    type: issueLinks[j].type.inward,
                                    key: issueLinks[j].inwardIssue.key,
                                    title: issueLinks[j].inwardIssue.fields.summary,
                                    status: issueLinks[j].inwardIssue.fields.status.name
                                };
                                thisIssue.issueLinksInward.push(inward);
                            }

                            ////////// Hämta outward(?) issues //////////
                            if (issueLinks[j].hasOwnProperty("outwardIssue")) {
                                var outward = {
                                    type: issueLinks[j].type.outward,
                                    key: issueLinks[j].outwardIssue.key,
                                    title: issueLinks[j].outwardIssue.fields.summary,
                                    status: issueLinks[j].outwardIssue.fields.status.name
                                };
                            }
                        }
                    }
                    if (projectName == "MAC") {

                        projectIssues.MACissues.push(thisIssue);
                    } else if (projectName == "MPP") {
                        projectIssues.MPPissues.push(thisIssue);
                    } else if (projectName == "MEM") {
                        projectIssues.MEMissues.push(thisIssue);
                    } else if (projectName == "MACC") {
                        projectIssues.MACCissues.push(thisIssue);
                    } else {}

                    //fixedJiraRelease.issues.push(thisIssue);
                }

            }

            return callback(projectIssues);
        }

    });

}


function getJiraReleasesWithinDateRange(project, currentWeek, values) {
    var releasesPlusMinThreeWeeks = [];
    var regExp = new RegExp("^(" + project + ")( )(201)([0-9])\.([0-5][0-9])");

    if (values) {
        for (var i = 0; i < values.length; i++) {
            if (values[i].name.match(regExp)) {
                var matchedString = values[i].name.match(regExp)[0];
                var withinDateRange = checkWithinDateRange(project, matchedString, 3);
                if (withinDateRange) {
                    releasesPlusMinThreeWeeks.push(values[i]);
                }
            }
        }
    }

    return releasesPlusMinThreeWeeks;
};

var getCurrentProjectReleaseDate = function(project, date) {
    var d = new Date(date);
    d.setHours(0, 0, 0);
    d.setDate(d.getDate() + 4 - (d.getDay() || 7));
    var week = Math.ceil((((d - new Date(d.getFullYear(), 0, 1)) / 8.64e7) + 1) / 7)
    return project + " " + d.getFullYear() + "." + week;
};

function checkWithinDateRange(project, releaseName, weeksBeforeAndAfter) {
    var thisWeek = new Date();

    var threeWeeksAgo = new Date(+thisWeek);
    threeWeeksAgo.setDate(threeWeeksAgo.getDate() - (7 * weeksBeforeAndAfter));

    var inThreeWeeks = new Date(+thisWeek);
    inThreeWeeks.setDate(inThreeWeeks.getDate() + (7 * weeksBeforeAndAfter));

    var threeAgo = getCurrentProjectReleaseDate(project, threeWeeksAgo);
    var inThree = getCurrentProjectReleaseDate(project, inThreeWeeks);
    var now = getCurrentProjectReleaseDate(project, thisWeek);

    if (releaseName <= inThree && releaseName >= threeAgo) {
        return true;
    } else {
        return false;
    }
}




module.exports = router;