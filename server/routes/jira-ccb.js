const express = require('express');
const router = express.Router();
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');

var async = require('async');
const axios = require('axios');



router.get('/jiraFixVersion/:id', function(req, res, next) {

    var id = req.params.id;

    var rp = require('request-promise');
    var opts = {
        url: "https://jira.scania.com/rest/api/2/version/"  + id.toString(),
        method: 'GET',
        json: true,
        headers: {
            Authorization: 'Basic XXX'
        }
    };

    rp(opts)
        .then(function(body) {
            var fixversion = body;
            res.send(fixversion);    
        })
        .catch(function(err) {
            console.log('rp (/jiraFixVersion/:id) failed for ' +  id.toString());
            console.log(err.statusCode);
            console.log(err.options);
   
        });
});

router.get('/jiraFixVersionIssues/:id', function(req, res, next) {

    var id = req.params.id;    
    var opts = {
        url: "https://jira.scania.com/rest/api/2/search?jql=fixVersion in ("  + id.toString() + ")",
        method: 'GET',
        json: true,
        headers: {
            Authorization: 'Basic XXX'
        }
    };
    var rp = require('request-promise');
    rp(opts)
        .then(function(body) {
            var issues = body.issues;
            res.send(issues); 
        })
        .catch (function(err2) {
            console.log('error jiraFixVersionIssues/:id' + id);
        })
});
        



module.exports = router;







