const express = require('express');
const router = express.Router();
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');

var async = require('async');
/* GET api listing. */

router.get('/jiraReleases', function(req, res, next) {

    var jiraReleaseUrls = [
        "http://jira.scania.com/rest/api/2/project/" + "MAC" + "/version?&maxResults=40&orderBy=-name",
        "http://jira.scania.com/rest/api/2/project/" + "MPP" + "/version?&maxResults=40&orderBy=-name",
        "http://jira.scania.com/rest/api/2/project/" + "MEM" + "/version?&maxResults=40&orderBy=-name",
    ];

    var jiraOptions = [];
    for (var i = 0; i < jiraReleaseUrls.length; i++) {
        var opts = {
            method: 'GET',
            json: true,
            url: jiraReleaseUrls[i],
            headers: {
                Authorization: 'Basic XXX'
            }
        };
        jiraOptions.push(opts)
    }


    var jiraReleases = [];
    async.map([
        jiraOptions[0],
        jiraOptions[1],
        jiraOptions[2]
    ], jira, function(err, results) {

        if (err) {
            console.log(err);
        }
        jiraReleasesMAC = results[0].values;
        jiraReleasesMPP = results[1].values;
        jiraReleasesMEM = results[2].values;

        var jiraMAC = [];
        var jiraMPP = [];
        var jiraMEM = [];


        jiraReleasesMAC = getJiraReleasesWithinDateRange("MAC", 3, jiraReleasesMAC);
        jiraReleasesMPP = getJiraReleasesWithinDateRange("MPP", 3, jiraReleasesMPP);
        jiraReleasesMEM = getJiraReleasesWithinDateRange("MEM", 3, jiraReleasesMEM);


        for (var i = 0; i < jiraReleasesMAC.length; i++) {
            jiraReleasesMAC[i].MACreleased = jiraReleasesMAC[i].released;
            if (jiraReleasesMAC[i].released) {
                jiraReleasesMAC[i].MACreleaseDate = jiraReleasesMAC[i].releaseDate;
            }
        }
        for (var i = 0; i < jiraReleasesMPP.length; i++) {
            jiraReleasesMPP[i].MPPreleased = jiraReleasesMPP[i].released;
            if (jiraReleasesMPP[i].released) {
                jiraReleasesMPP[i].MPPreleaseDate = jiraReleasesMPP[i].releaseDate;
            }
        }
        for (var i = 0; i < jiraReleasesMEM.length; i++) {
            jiraReleasesMEM[i].MEMreleased = jiraReleasesMEM[i].released;
            if (jiraReleasesMEM[i].released) {
                jiraReleasesMEM[i].MEMreleaseDate = jiraReleasesMEM[i].releaseDate;
            }
        }


        var joinedReleases = jiraReleasesMAC.concat(jiraReleasesMPP, jiraReleasesMEM);

        for (var i = 0; i < joinedReleases.length; i++) {
            joinedReleases[i].releaseName = joinedReleases[i].name.replace("MAC ", '').replace("MPP ", '').replace('MEM ', '');
        }


        var consolidatedReleases = [];
        var resps = 0;
        for (var i = 0; i < joinedReleases.length; i++) {

            jiraReleatedIssues(joinedReleases[i].name.substring(0, 3), joinedReleases[i], function(resp) {

                resps += 1;

                consolidatedReleases.push(resp);
                if (i == resps) {

                    res.send(consolidatedReleases); //return consolidatedReleases;
                }
            });

        }

    });

}); // router get slut


var jira = function(apiReqOptions, okCallback) {
    var request = require('request');
    request(apiReqOptions, function(error, response, body) {
        if (error) {
            console.log(error);
        } else {
            return okCallback(null, body);
        }
    });
};



function jiraReleatedIssues(projectName, jiraRelease, callback) {
    var jiraRelatedUrl = "https://jira.scania.com/rest/api/2/search?jql=project=" + projectName + " and fixVersion=" + jiraRelease.id;
    var jiraIssuesInReleaseOptions = {
        method: 'GET',
        json: true,
        url: jiraRelatedUrl,
        headers: {
            Authorization: 'Basic XXX'
        }
    }

    var request = require('request');

    request(jiraIssuesInReleaseOptions, function(error, response, body) {
        if (error) {
            console.log(error);
        } else {
            var fixedJiraRelease = {
                releaseName: jiraRelease.name.replace("MAC ", '').replace("MPP ", '').replace('MEM ', '') //,
                    //issues: []
            };
            if (jiraRelease.name.includes("MAC")) {
                fixedJiraRelease.MACissues = [];

            } else if (jiraRelease.name.includes("MPP")) {
                fixedJiraRelease.MPPissues = [];

            } else if (jiraRelease.name.includes("MEM")) {
                fixedJiraRelease.MEMissues = [];

            }

            var issues = body.issues;
            if (issues) {

                for (var i = 0; i < issues.length; i++) {

                    var thisIssue = {};

                    ////////// Hämta all basinfo och strukturera issues //////////
                    thisIssue = {
                        key: issues[i].key,
                        url: "https://jira.scania.com/browse/" + issues[i].key,
                        title: issues[i].fields.summary,
                        status: issues[i].fields.status.name, //issues[i].fields.status.statusCategory.name,
                        statusIcon: issues[i].fields.status.iconUrl,

                        type: issues[i].fields.issuetype.name,
                        typeIcon: issues[i].fields.issuetype.iconUrl,
                        labels: [],
                        components: [],
                        // valueItems: [],
                        isInGoRelease: true,

                        serviceCenterId: [],
                        priority: issues[i].fields.priority.name,
                        priorityIcon: issues[i].fields.priority.iconUrl,

                        description: issues[i].fields.description,
                        reporter: issues[i].fields.reporter.displayName,
                        additionalAssignees: [],
                        issueLinksInward: [],
                        issueLinksOutward: []
                    };
                    if (issues[i].fields.assignee) {
                        thisIssue.assignee = issues[i].fields.assignee.displayName;
                    } else {
                        thisIssue.assignee = 'Unassigned';
                    }


                    if (issues[i].fields.customfield_13386) {
                        thisIssue.valueItem = issues[i].fields.customfield_13386;
                    }
                    if (issues[i].fields.customfield_10111) {
                        thisIssue.severity = issues[i].fields.customfield_10111;
                    }

                    var tempCreated = issues[i].fields.created.substring(0, issues[i].fields.created.search(/\./));
                    var tempUpdated = issues[i].fields.updated.substring(0, issues[i].fields.updated.search(/\./));

                    thisIssue.created = {
                        date: tempCreated.split("T")[0],
                        time: tempCreated.split("T")[1]
                    };
                    thisIssue.updated = {
                        date: tempUpdated.split("T")[0],
                        time: tempUpdated.split("T")[1]
                    };

                    if (issues[i].fields.description) {
                        var techStart = "<Release Notes Technical Start>";
                        var techEnd = "<Release Notes Technical End>";
                        var description = issues[i].fields.description.replace(/\*/g, '');

                        var businessValueItemString = "business value item:";
                        var backgroundString = "background:";
                        var solutionString = "solution:";
                        var affectsUserString = "affects user:";

                        if (description.search(techStart) != -1 && description.search(techEnd) != -1) {

                            var techStripped = description.substring(description.search(techStart) + techStart.length + 1, description.search(techEnd) - description.search(techStart) + techStart.length + techEnd.length + 1);

                            var end = description.search(techEnd);
                            var start = description.search(techStart);
                            var len = (end - start);
                            var str = description.substr(start + techStart.length, len - techStart.length);

                            thisIssue.releaseNotesTechnical = str;


                            var businessValueItemStart = str.toLowerCase().search(businessValueItemString);
                            var backgroundStart = str.toLowerCase().search(backgroundString);
                            var solutionStart = str.toLowerCase().search(solutionString);
                            var affectsUserStart = str.toLowerCase().search(affectsUserString);

                            if (businessValueItemStart != -1 && backgroundStart != -1 && solutionStart != -1 && affectsUserStart != -1) {
                                var businessRes = str.substring(businessValueItemStart + businessValueItemString.length, backgroundStart).trim();
                                var backgroundRes = str.substring(backgroundStart + backgroundString.length, solutionStart).trim();
                                var solutionRes = str.substring(solutionStart + solutionString.length, affectsUserStart).trim();
                                var affectsUserRes = str.substring(affectsUserStart + affectsUserString.length, str.length).trim();

                                thisIssue.notesTechnical = {
                                    businessValueItem: businessRes,
                                    background: backgroundRes,
                                    solution: solutionRes,
                                    affectsUser: affectsUserRes
                                }

                            }

                        }

                        var commercialStart = "<Release Notes Commercial Start>";
                        var commercialEnd = "<Release Notes Commercial End>";

                        if (description.search(commercialStart) != -1 && description.search(commercialEnd) != -1) {

                            var techStripped = description.substring(description.search(commercialStart) + techStart.length + 1, description.search(commercialEnd) - description.search(commercialStart) + commercialStart.length + commercialEnd.length + 1);

                            var end = description.search(commercialEnd);
                            var start = description.search(commercialStart);
                            var len = (end - start);
                            var str = description.substr(start + commercialStart.length, len - commercialStart.length);

                            thisIssue.releaseNotesCommercial = str;


                            var businessValueItemStart = str.toLowerCase().search(businessValueItemString);


                            var backgroundStart = str.toLowerCase().search(backgroundString);


                            var solutionStart = str.toLowerCase().search(solutionString);


                            var affectsUserStart = str.toLowerCase().search(affectsUserString);

                            if (businessValueItemStart != -1 && backgroundStart != -1 && solutionStart != -1 && affectsUserStart != -1) {
                                var businessRes = str.substring(businessValueItemStart + businessValueItemString.length, backgroundStart).trim();
                                var backgroundRes = str.substring(backgroundStart + backgroundString.length, solutionStart).trim();
                                var solutionRes = str.substring(solutionStart + solutionString.length, affectsUserStart).trim();
                                var affectsUserRes = str.substring(affectsUserStart + affectsUserString.length, str.length).trim();

                                thisIssue.notesCommercial = {
                                    businessValueItem: businessRes,
                                    background: backgroundRes,
                                    solution: solutionRes,
                                    affectsUser: affectsUserRes
                                }


                            }
                        }

                    }

                    ////////// Hämta labels //////////
                    if (issues[i].fields.labels.length > 0) {
                        for (var m = 0; m < issues[i].fields.labels.length; m++) {

                            var label = issues[i].fields.labels[m];
                            if (label) {
                                thisIssue.labels.push(label);
                            }
                        }
                    }

                    ////////// Hämta components //////////
                    if (issues[i].fields.components.length > 0) {
                        for (var m = 0; m < issues[i].fields.components.length; m++) {
                            //var component = issues[i].fields.components[m].name;

                            thisIssue.components.push(issues[i].fields.components[m].name);
                        }
                    }

                    ////////// Hämta additional assignees //////////
                    if (issues[i].fields.customfield_13295) {
                        for (var x = 0; x < issues[i].fields.customfield_13295.length; x++) {
                            thisIssue.additionalAssignees.push(issues[i].fields.customfield_13295[x].displayName);
                        }
                    }

                    // External ref. (service center)
                    if (issues[i].fields.customfield_10106) {

                        var serviceCenterId = issues[i].fields.customfield_10106.split(" ");
                        for (var j = 0; j < serviceCenterId.length; j++) {
                            thisIssue.serviceCenterId.push(serviceCenterId[j]);
                        }

                    }

                    ////////// Hämta länkade issues //////////
                    var issueLinks = issues[i].fields.issuelinks;
                    if (issueLinks) {

                        for (var j = 0; j < issueLinks.length; j++) {

                            ////////// Hämta inward(?) issues //////////
                            if (issueLinks[j].hasOwnProperty("inwardIssue")) {
                                var inward = {
                                    type: issueLinks[j].type.inward,
                                    key: issueLinks[j].inwardIssue.key,
                                    title: issueLinks[j].inwardIssue.fields.summary,
                                    status: issueLinks[j].inwardIssue.fields.status.name
                                };
                                thisIssue.issueLinksInward.push(inward);
                            }

                            ////////// Hämta outward(?) issues //////////
                            if (issueLinks[j].hasOwnProperty("outwardIssue")) {
                                var outward = {
                                    type: issueLinks[j].type.outward,
                                    key: issueLinks[j].outwardIssue.key,
                                    title: issueLinks[j].outwardIssue.fields.summary,
                                    status: issueLinks[j].outwardIssue.fields.status.name
                                };
                            }
                        }
                    }
                    if (jiraRelease.name.includes("MAC")) {
                        fixedJiraRelease.MACissues.push(thisIssue);
                        fixedJiraRelease.MACreleased = jiraRelease.released;
                        if (jiraRelease.released) {
                            fixedJiraRelease.MACreleaseDate = jiraRelease.releaseDate;
                        }
                    } else if (jiraRelease.name.includes("MPP")) {
                        fixedJiraRelease.MPPissues.push(thisIssue);
                        fixedJiraRelease.MPPreleased = jiraRelease.released;
                        if (jiraRelease.released) {
                            fixedJiraRelease.MPPreleaseDate = jiraRelease.releaseDate;
                        }
                    } else if (jiraRelease.name.includes("MEM")) {
                        fixedJiraRelease.MEMissues.push(thisIssue);
                        fixedJiraRelease.MEMreleased = jiraRelease.released;
                        if (jiraRelease.released) {
                            fixedJiraRelease.MEMreleaseDate = jiraRelease.releaseDate;
                        }
                    } else {}

                }

            }

            return callback(fixedJiraRelease);
        }

    });

}


function getJiraReleasesWithinDateRange(project, currentWeek, values) {
    var releasesPlusMinThreeWeeks = [];
    var regExp = new RegExp("^(" + project + ")( )(201)([0-9])\.([0-5][0-9])");

    if (values) {
        for (var i = 0; i < values.length; i++) {
            if (values[i].name.match(regExp)) {
                var matchedString = values[i].name.match(regExp)[0];
                var withinDateRange = checkWithinDateRange(project, matchedString, 3);
                if (withinDateRange) {
                    releasesPlusMinThreeWeeks.push(values[i]);
                }
            }
        }
    }
    return releasesPlusMinThreeWeeks;
};

var getCurrentProjectReleaseDate = function(project, date) {
    var d = new Date(date);
    d.setHours(0, 0, 0);
    d.setDate(d.getDate() + 4 - (d.getDay() || 7));
    var week = Math.ceil((((d - new Date(d.getFullYear(), 0, 1)) / 8.64e7) + 1) / 7).toString();
    if (week.length < 2) {
        week = '0' + week;
    }
    return project + " " + d.getFullYear() + "." + week;
};

function checkWithinDateRange(project, releaseName, weeksBeforeAndAfter) {
    var thisWeek = new Date();

    var threeWeeksAgo = new Date(+thisWeek);
    threeWeeksAgo.setDate(threeWeeksAgo.getDate() - (7 * weeksBeforeAndAfter));

    var inThreeWeeks = new Date(+thisWeek);
    inThreeWeeks.setDate(inThreeWeeks.getDate() + (7 * weeksBeforeAndAfter));

    var threeAgo = getCurrentProjectReleaseDate(project, threeWeeksAgo);
    var inThree = getCurrentProjectReleaseDate(project, inThreeWeeks);
    var now = getCurrentProjectReleaseDate(project, thisWeek);

    if (releaseName <= inThree && releaseName >= threeAgo) {
        return true;
    } else {
        return false;
    }
}

module.exports = router;