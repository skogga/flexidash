const express = require('express');
const router = express.Router();
var mongojs = require('mongojs');


router.get('/', (req, res) => {
    res.send('api works');
});


router.get('/slackTemplates', function(req, res, next) {
    // Retrieve
    var MongoClient = require('mongodb').MongoClient,
        assert = require('assert');
    var url = 'mongodb://127.0.0.1:27017/flexDB';
    // Connect to the db
    MongoClient.connect(url, function(err, db) {
        if (!err) {

            var collection = db.collection('slackTemplates');

            //collection.find({ 'supportType': 'Open' }).toArray(function(err, docs) {
            collection.find().toArray(function(err, docs) {
                assert.equal(null, err);
                //assert.equal(2, docs.length);

                res.json(docs);
            });
        }
        //db.close();
    });
});

// Update supportCase
router.put('/slackTemplate/:id', function(req, res, next) {

    var MongoClient = require('mongodb').MongoClient,
        assert = require('assert');
    var url = 'mongodb://127.0.0.1:27017/flexDB';
    // Connect to the db
    var slackTemplate = req.body;
    var updSlackTemplate = {};

    if (slackTemplate.messageTemplate) {
        updSlackTemplate.messageTemplate = slackTemplate.messageTemplate;
    }
    if (slackTemplate.messageType) {
        updSlackTemplate.messageType = slackTemplate.messageType;
    }

    if (!updSlackTemplate) {
        //res.status(400);
        console.log("error in slack-templates.js");
        res.json({
            "error": "Bad data"
        });
    } else {

        MongoClient.connect(url, function(err, db) {
            if (!err) {
                var collection = db.collection('slackTemplates');

                collection.update({ _id: mongojs.ObjectId(req.params.id) }, updSlackTemplate, {}, function(err, slackTemplate) {
                    assert.equal(null, err);

                    if (err) {
                        console.log("error in slack-templates.js");
                    }
                });
            }
        });
    }
});

module.exports = router;