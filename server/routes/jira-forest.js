const express = require('express');
const router = express.Router();
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');

var async = require('async');



router.get('/jiraForest', function(req, res, next) {

    var options = {
        url: "https://jira.scania.com/rest/structure/2.0/forest/latest?s={%22structureId%22:371}",
        method: 'GET',
        json: true,
        headers: {
            Authorization: 'Basic XXX'

        }
    };

    var rp = require('request-promise');
    rp(options)
        .then(function(body) {

            const formulas = body.formula.split(',');

            const rows = [];
            for (let i = 0; i < formulas.length; i++) {

                const obj = {
                    rowId: formulas[i].substring(0, formulas[i].indexOf(':')),
                    rowDepth: formulas[i].substring(formulas[i].indexOf(":") + 1, formulas[i].lastIndexOf(":")),
                    itemId: formulas[i].substring(formulas[i].lastIndexOf(":") + 1, formulas[i].length)
                }
                rows.push(obj);
            }

            const rowIds = rows.map(a => a.rowId);

            const opts = {
                method: 'POST',
                uri: "https://jira.scania.com/rest/structure/2.0/value.json",

                json: true,
                headers: {
                    Authorization: 'Basic XXX'
                },
                data: {
                    "requests": [{
                        "forestSpec": {
                            "structureId": 371

                        },
                        "rows": rowIds,
                        "attributes": [{
                                "id": "summary",
                                "format": "text"
                            },
                            {
                                "id": "key",
                                "format": "text" /* html */
                            },
                            {
                                "id": "progress",
                                "format": "number",
                                "params": {
                                    "basedOn": "timetracking",
                                    "resolvedComplete": true,
                                    "weightBy": "equal"
                                }
                            }
                        ]
                    }]
                }
            };

            var rp2 = require('request-promise');
            var options2 = {
                method: 'POST',
                uri: 'https://jira.scania.com/rest/structure/2.0/value',
                headers: {
                    Authorization: 'Basic XXX'
                },
                body: opts.data,
                json: true // Automatically stringifies the body to JSON
            };
            var entriesInStructure = [];
            rp2(options2)
                .then(function(body) {

                    for (let i = 0; i < body.responses[0].data[0].values.length; i++) {

                        const entry = {
                            name: body.responses[0].data[0].values[i],
                            sort: body.responses[0].data[1].values[i],
                            levelId: parseInt(rows[i].rowDepth),
                            itemId: rows[i].itemId
                        }


                        entriesInStructure.push(entry);
                    }
                    const treesWithIssues = entriesInStructure.filter(tree => tree.sort);

                    forestKeys = [];

                    treesWithIssues.forEach(function(element) {
                        forestKeys.push(element.sort);
                    });


                    var rp3 = require('request-promise');
                    var options3 = {
                        method: 'GET',
                        uri: 'https://jira.scania.com/rest/api/2/search?jql=key in (' + forestKeys + ')&maxResults=200',
                        headers: {
                            Authorization: 'Basic XXX'
                        },
                        json: true
                    };

                    rp3(options3)
                        .then(function(body) {

                            var fields = body.issues;

                            var treesWithFields = entriesInStructure.map(function(entry) {
                                if (fields.findIndex(x => x.key == entry.sort) != -1) {
                                    entry.fields = fields[fields.findIndex(x => x.key == entry.sort)].fields;
                                }

                                return entry;
                            });

                            var result = [],
                                levels = [result];
                            treesWithFields.forEach(({ levelId, name, sort, fields, itemId }) =>
                                levels[levelId].push({ levelId, name, sort, fields, itemId, children: levels[levelId + 1] = [] })
                            );

                            res.send(result);

                        })
                        .catch(function(err) {

                            console.log('error in rp3 (get forest) ');
                            console.log(err);
                        });

                })
                .catch(function(err) {
                    console.log('error in rp2 (get forest) ');
                    console.log(err);
                });
        })
        .catch(function(err) {
            console.log('error in rp (get forest)');
            console.log(err);
        });
});


module.exports = router;