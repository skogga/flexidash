import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptionsArgs } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class SlackMessagesService {

  constructor(private http: Http) {

  }



  getSlackMessages() {
    return this.http.get('http://RD0049523/api/slackMessages')
        .map(res => res.json());
  }
}
