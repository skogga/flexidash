import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptionsArgs } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class JiraReleaseService {

  constructor(private http: Http) {}

    getJiraReleases() {

        return this.http.get('http://RD0049523/api/jiraReleases')
            .map(res => res.json());
    }
}
