import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Http, Response, Headers, URLSearchParams, RequestOptions  } from '@angular/http';
import {BrowserModule} from '@angular/platform-browser';
import {JiraReleaseService} from '../jira-release.service';


@Component({
  moduleId: module.id,
  selector: 'app-jira-release',
  templateUrl: './jira-release.component.html',
  styleUrls: ['./jira-release.component.css']
})
export class JiraReleaseComponent implements OnInit {

  constructor(private jiraReleaseService: JiraReleaseService) {

  }


  jiraReleases = [];

  ngOnInit() {
    this.getJiraReleases();
    setInterval(() => {
       this.getJiraReleases();
    }, 60000);
  }
  print(): void {
    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
}

  trackByReleaseName(index, rel) {
    return rel.releaseName;
  }

  trackByIssueUpdated(index, iss) {
    return iss.updated.time;
  }

  inList(arr, obj) {
    let inArr = false;
    for (let i = 0; i < arr.length; i++ ) {
      if (obj.releaseName === arr[i].releaseName) {
        inArr = true;
        if (obj.hasOwnProperty('MACissues')) {
                arr[i].MACissues = obj.MACissues;
        } else if (obj.hasOwnProperty('MPPissues')) {
                arr[i].MPPissues = obj.MPPissues;
        } else if (obj.hasOwnProperty('MEMissues')) {
                arr[i].MEMissues = obj.MEMissues;
        }
      }
    }
    if (!inArr) {
      arr.push(obj);
    }
  }

  arraySort(a, b) {
    if (a.releaseName < b.releaseName) {
      return -1;
    }
    if (a.releaseName > b.releaseName) {
      return 1;
    }
    return 0;
  }



  getJiraReleases() {
    // console.log('in jira-release component');
    this.jiraReleaseService.getJiraReleases()
        .subscribe(releasesSeparated => {

            const joinedArray = [];

            for (let i = 0; i < releasesSeparated.length; i++) {
              this.inList(joinedArray, releasesSeparated[i]);
            }

            joinedArray.sort(this.arraySort);
            this.jiraReleases = joinedArray;

        },
        (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              console.log('client side error occurred');
            } else {
              console.log('server side error');
            }
        });
  }

}







/* 
var string = "Full recalculate sets status to replaced if factory asdfasdf";


var result;
if (string.length > 50){
	result = string.substring(0,47);
	result += "...";
}
else {
	results = string;
}

result;

*/