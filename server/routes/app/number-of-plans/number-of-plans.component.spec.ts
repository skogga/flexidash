import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NumberOfPlansComponent } from './number-of-plans.component';

describe('NumberOfPlansComponent', () => {
  let component: NumberOfPlansComponent;
  let fixture: ComponentFixture<NumberOfPlansComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NumberOfPlansComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumberOfPlansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
