

import { Component, OnInit, Input  } from '@angular/core';
import {SlackTemplatesService} from '../slack-templates.service';
import {SlackTemplate} from '../../SlackTemplate';
import { HttpClient } from '@angular/common/http';
import { Http, Response } from '@angular/http';
import {BrowserModule} from '@angular/platform-browser';
@Component({
    moduleId: module.id,
    selector: 'app-slack-templates',
    templateUrl: './slack-templates.component.html',
    styleUrls: ['./slack-templates.component.css']
})
export class SlackTemplatesComponent implements OnInit  {
    slackTemplates: SlackTemplate[];
    messageType: string;
    messageTemplate: string;
    isEditingTemplate: boolean;

    isPostingGoNoGoMessage = false;
    isPostingAbscenceMessage = false;
    isPostingWorkFromHomeMessage = false;
    isPostingCustomMessage = false;
    isPostingToSlack = false;
/*
    isEditingGoTemplate = false;
    isEditingNoGoTemplate = false; */
   
    goTemplate: SlackTemplate;
    noGoTemplate: SlackTemplate;
    abscenceTemplate: SlackTemplate;
    workFromHomeTemplate: SlackTemplate;


    // build prod stuff
    editingAbscenceTemplate = false;
    editingGoTemplate = false;
    editingWorkFromHomeTemplate = false;
    editingNoGoTemplate = false;
    // build prod stuff


    @Input()  name: string;
    @Input()  noGoInput: string;
    @Input()  goInput: string;
    @Input()  customMessageInput: string;
    @Input()  workFromHomeInput: string;
    @Input() abscenceInput: string;


    constructor(private slackTemplateService: SlackTemplatesService) {
        this.isEditingTemplate = false;
    }

    getSlackTemplates() {
        this.slackTemplateService.getSlackTemplates()
        .subscribe(slackTemplates => {
            this.slackTemplates = slackTemplates;

            for (let i = 0; i < slackTemplates.length; i++) {
                const messageType = slackTemplates[i].messageType;
                // console.log("Template: ");
                // console.log(slackTemplates[i]);
                // console.log("");
                if (messageType === 'goMessage') {
                    this.goTemplate = slackTemplates[i]; // .messageTemplate;
                }else if (messageType === 'noGoMessage') {
                    this.noGoTemplate = slackTemplates[i];
                }else if (messageType === 'abscenceMessage') {
                    this.abscenceTemplate = slackTemplates[i];
                }else if (messageType === 'workFromHomeMessage') {
                    this.workFromHomeTemplate = slackTemplates[i];
                }
            }
        });
    }

    ngOnInit() {
        this.getSlackTemplates();
    }
    updateMessageTemplate(slackTemplate, newMessage) {
        const _slackTemplate = {
            _id: slackTemplate._id,
            messageType: slackTemplate.messageType,
            messageTemplate: newMessage // slackTemplate.messageTemplate
        };
        this.slackTemplateService.updateMessageTemplate(_slackTemplate).subscribe(data => {
            this.messageTemplate = _slackTemplate.messageTemplate;
            this.messageType = _slackTemplate.messageType;
        });
        this.getSlackTemplates();
    }

    sendToSlack(incomingTemplate) {
        let fixed;

        // Om är ett template
        if (incomingTemplate.hasOwnProperty('messageTemplate')) {
            if (incomingTemplate.messageTemplate.includes('<TIME>') || incomingTemplate.messageTemplate.includes('<NAME>') || incomingTemplate.messageTemplate.includes('<DATE>')) {
            fixed = incomingTemplate.messageTemplate.replace('<TIME>', this.TimeFixer()).replace('<NAME>', 'namechange').replace('<DATE>', this.DateFixer());
            incomingTemplate = fixed;
        } else {
            fixed = incomingTemplate;
            }
        }
        // Om custom message
        else {
            fixed = incomingTemplate;
            // nollställ textfältet:
            this.customMessageInput = '';
        }
        this.slackTemplateService.sendToSlack(fixed);
    }

    TimeFixer() {
        const d = new Date();
        let datetext = d.toTimeString();
        datetext = datetext.split(' ')[0];
        return datetext;
    }

    DateFixer() {
        var date = new Date();
        var year = date.getFullYear().toString();
        var month = (date.getMonth() + 1).toString();

        if (month.toString().length < 2) {
            month = "0" + month;
        }

        var day = date.getDate().toString();
        var fullDate = year + "/" + month + "/" + day;

        return fullDate;
    }


}
