import { Component, OnInit } from '@angular/core';
import {SupportCaseService} from '../support-cases.service';
import {SupportCase} from '../../SupportCase';

@Component({
    moduleId: module.id,
    selector: 'app-support-cases',
    templateUrl: 'support-cases.component.html'
})
export class SupportCasesComponent {
    supportCases: SupportCase[];
    number: string;
    supportType: string;
    isEditingSupportCase: boolean;

    constructor(private supportCaseService: SupportCaseService) {
        this.isEditingSupportCase = false;
        this.supportCaseService.getSupportCases()
        .subscribe(supportCases => {
            this.supportCases = supportCases;
        });
    }

    updateSupportNumber(supportCase) {
        var _supportCase = {
            _id: supportCase._id,
            number: supportCase.number,
            supportType: supportCase.supportType
        };

        this.supportCaseService.updateSupportNumber(_supportCase).subscribe(data => {
            this.number = _supportCase.number;
            this.supportType = _supportCase.supportType;
        })
    }
}
