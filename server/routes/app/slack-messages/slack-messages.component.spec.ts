import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlackMessagesComponent } from './slack-messages.component';

describe('SlackMessagesComponent', () => {
  let component: SlackMessagesComponent;
  let fixture: ComponentFixture<SlackMessagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlackMessagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlackMessagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
