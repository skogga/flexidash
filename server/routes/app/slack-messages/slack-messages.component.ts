import { Component, OnInit, Input } from '@angular/core';

import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Http, Response, Headers, URLSearchParams, RequestOptions  } from '@angular/http';
import {BrowserModule} from '@angular/platform-browser';


import {SlackMessagesService} from '../slack-messages.service';
import { SlackTemplatesComponent} from '../slack-templates/slack-templates.component';
interface MessageResponse {
  members: JSON;
}
@Component({
  selector: 'app-slack-messages',
  templateUrl: './slack-messages.component.html',
  styleUrls: ['./slack-messages.component.css']
})
export class SlackMessagesComponent implements OnInit {
  messages = [];
  userInfo = [];
  channelInfo = [];
  teamIcon = '';

  constructor (private slackMessagesService: SlackMessagesService) {}


  ngOnInit() {
   // this.getSlackMessages();
    /*
    setInterval(() => {

      this.getSlackMessages();
    }, 10000);

    */

  }

  @Input()
  set ready(isReady: boolean) {
    if (isReady) {
      this.scrollDown();
    }
  }


  finisedLoadingSlack() {
    // console.log('done loading stuff..');
  }

  trackByTs(index, mess) {
    return mess.ts;
  }

  getSlackMessages() {

    this.slackMessagesService.getSlackMessages()
        .subscribe(slackHistory => {

          // console.log('Length of receive: ' + slackHistory.length);
          if (slackHistory.length > 0){
              var sortedHistory = slackHistory.sort(this.SortTimeStamp);
              console.log(sortedHistory);
              this.messages = sortedHistory;
          }
        },
        (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              console.log('client side error occurred');
            } else {
              console.log('server side error');
            }
        });
  }

  SortTimeStamp(a, b) {
    if (a.ts < b.ts)
        return -1;
    if (a.ts > b.ts)
        return 1;
    return 0;
  }


/* 
select top 10 OrganizationLevelId, SUM(Price) as Sumprices
from PartPrice
group by OrganizationLevelId
order by Sumprices asc
3E6DA124-0E89-41CC-B5EC-089C0CBB4C14
*/

  scrollDown(){
    // Scroll down the feed
    var scrollHeight = document.getElementById("scrollableBodySlack").scrollHeight;
    //console.log("scrolltop: " + scrollHeight);
    document.getElementById("scrollableBodySlack").scrollTop = 10000000;
  }



}
