import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { SlackMessagesComponent } from './slack-messages/slack-messages.component';
import { SlackTemplatesComponent } from './slack-templates/slack-templates.component';
import { SupportCasesComponent } from './support-cases/support-cases.component';


import { FormsModule } from '@angular/forms';
import { TeamIconDirective } from './team-icon.directive';
import { JiraReleaseComponent } from './jira-release/jira-release.component';
import {CustExtBrowserXhr} from './cust-ext-browser-xhr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsModalModule } from 'ng2-bs3-modal';
import { TextLengthPipe } from './text-length.pipe';
import { NumberOfPlansComponent } from './number-of-plans/number-of-plans.component';
import { WhatsUpComponent } from './whats-up/whats-up.component';
import { JiraPulsComponent } from './jira-puls/jira-puls.component';

@NgModule({
  declarations: [
    AppComponent,
    SlackMessagesComponent,
    SlackTemplatesComponent,
    SupportCasesComponent,
    TeamIconDirective,
    JiraReleaseComponent,
    TextLengthPipe,
    NumberOfPlansComponent,
    WhatsUpComponent,
    JiraPulsComponent
  ],
  imports: [
    BrowserModule, HttpModule, FormsModule, BrowserAnimationsModule, BsModalModule
  ],
  providers: [],
  bootstrap: [AppComponent],

})
export class AppModule { }
