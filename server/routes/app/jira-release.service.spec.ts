import { TestBed, inject } from '@angular/core/testing';

import { JiraReleaseService } from './jira-release.service';

describe('JiraReleaseService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JiraReleaseService]
    });
  });

  it('should be created', inject([JiraReleaseService], (service: JiraReleaseService) => {
    expect(service).toBeTruthy();
  }));
});
