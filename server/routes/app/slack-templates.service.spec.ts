import { TestBed, inject } from '@angular/core/testing';

import { SlackTemplatesService } from './slack-templates.service';

describe('SlackTemplatesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SlackTemplatesService]
    });
  });

  it('should be created', inject([SlackTemplatesService], (service: SlackTemplatesService) => {
    expect(service).toBeTruthy();
  }));
});
