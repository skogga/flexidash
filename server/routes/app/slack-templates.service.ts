
import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class SlackTemplatesService {
    constructor(private http: Http) {
        // console.log('Slack Template service initialized');
    }

    getSlackTemplates() {
        return this.http.get('http://RD0049523/api/slackTemplates')
        .map(res => res.json());
    }

    sendToSlack(template) {
        const mess = template;
        // console.log('mess in service: ' + mess);
        const headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        const data = {
                    payload: {
                        text: mess,
                        username: 'FlexBot',
                        as_user: true,
                        icon_emoji: ':postal_horn:' //':robot_face:'
                    }
        };
        var webHooks = {
            FlexMgmt: "https://hooks.slack.com/services/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",
            NodeMPP: "https://hooks.slack.com/services/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",
            NodeMAC: "https://hooks.slack.com/services/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",
            NodeSupport: 'https://hooks.slack.com/services/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'
        }
      
        // const dat = {payload:{"text":"test", "username":"PostmanTester", "icon_emoji": ":robot_face:"}};
        this.http.post(webHooks.NodeMPP, data.payload, {headers: headers}).subscribe();
        this.http.post(webHooks.NodeMAC, data.payload, {headers: headers}).subscribe();
        this.http.post(webHooks.NodeSupport, data.payload, {headers: headers}).subscribe();
        this.http.post(webHooks.FlexMgmt, data.payload, {headers: headers}).subscribe();
    }

    updateMessageTemplate(slackTemplate) {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.put('http://RD0049523/api/slackTemplate/' + slackTemplate._id, JSON.stringify(slackTemplate), {headers: headers})
            .map(res => res.json());
    }


}
