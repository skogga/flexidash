import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JiraPulsComponent } from './jira-puls.component';

describe('JiraPulsComponent', () => {
  let component: JiraPulsComponent;
  let fixture: ComponentFixture<JiraPulsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JiraPulsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JiraPulsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
