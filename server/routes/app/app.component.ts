import { Component, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';

import { SlackTemplatesService } from './slack-templates.service';
import { SlackMessagesService } from './slack-messages.service';
import { JiraReleaseService } from './jira-release.service';
import { SupportCaseService } from './support-cases.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [SlackTemplatesService, SupportCaseService, JiraReleaseService, SlackMessagesService]
})
export class AppComponent {
  title = 'app';
}
