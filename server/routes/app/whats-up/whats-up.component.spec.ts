import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhatsUpComponent } from './whats-up.component';

describe('WhatsUpComponent', () => {
  let component: WhatsUpComponent;
  let fixture: ComponentFixture<WhatsUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhatsUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhatsUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
