import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class SupportCaseService{
    constructor(private http:Http){
        // console.log("supportCase service initialized");
    }

    getSupportCases(){
        return this.http.get('http://RD0049523/api/supportCases')
        .map(res => res.json());
    }

    updateSupportNumber(supportCase){
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.put('http://RD0049523/api/supportCase/' + supportCase._id, JSON.stringify(supportCase), {headers: headers})
            .map(res => res.json())
    }
}
