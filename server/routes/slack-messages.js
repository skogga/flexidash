const express = require('express');
const router = express.Router();
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');

var async = require('async');



/**************************Flex Management****************************/

var flexManagement = {
    channelId: "y",
    oAuth: "x",
    webhookUrl: "https://hooks.slack.com/services/xxx",
    historyOptions: {
        url: "https://slack.com/api/channels.history?token=" + "xxx" + "&channel=" + "xxx" + "&count=" + 20 + "&latest=#{last}",
        method: 'GET',
        json: true
    },
    channelIconOptions: {
        url: "https://slack.com/api/team.info?token=" + "xxx",
        method: 'GET',
        json: true
    },
    userListOptions: {
        url: "https://slack.com/api/users.list?&token=" + "xxx",
        method: 'GET',
        json: true
    }
};
/**************************Flex Management****************************/

/************************* Super MAC *********************** */
var superMac = {
    channelId: "xxx",
    oAuth: "xxx",
    webhookUrl: "https://hooks.slack.com/services/...",

    historyOptions: {
        url: "https://slack.com/api/channels.history?token=" + "xxx" + "&channel=" + "xxx" + "&count=" + 20 + "&latest=#{last}",
        method: 'GET',
        json: true
    },
    channelIconOptions: {
        url: "https://slack.com/api/team.info?token=" + "xxx",
        method: 'GET',
        json: true
    },
    userListOptions: {
        url: "https://slack.com/api/users.list?&token=" + "xxx",
        method: 'GET',
        json: true
    }
};

/************************* MAC Cleod *********************** */


var macCloud = {
    channelId: "xxx",
    oAuth: "xxx",
    webhookUrl: "https://hooks.slack.com/services/...",
    historyOptions: {
        url: "https://slack.com/api/channels.history?token=" + "xxx" + "&channel=" + "xxx" + "&count=" + 20 + "&latest=#{last}",
        method: 'GET',
        json: true
    },
    channelIconOptions: {
        url: "https://slack.com/api/team.info?token=" + "xxx",
        method: 'GET',
        json: true
    },
    userListOptions: {
        url: "https://slack.com/api/users.list?&token=" + "xxx",
        method: 'GET',
        json: true,
    }
};


var flexSupport = {
    channelId: "xxx",
    oAuth: xxx,
    webhookUrl: "https://hooks.slack.com/services/...",
    historyOptions: {
        url: "https://slack.com/api/channels.history?token=" + "xxx" + "&channel=" + "C7ZHDUU6A" + "&count=" + 20 + "&latest=#{last}",
        method: 'GET',
        json: true
    },
    channelIconOptions: {
        url: "https://slack.com/api/team.info?token=" + "xxx",
        method: 'GET',
        json: true
    },
    userListOptions: {
        url: "https://slack.com/api/users.list?&token=" + "xxx",
        method: 'GET',
        json: true,
    }
};

var flexMEM = {
    channelId: "xxx",
    oAuth: "xxx",
    webhookUrl: "https://hooks.slack.com/services/.",
    historyOptions: {
        url: "https://slack.com/api/channels.history?token=" + "xxx" + "&channel=" + "xxx" + "&count=" + 20 + "&latest=#{last}",
        method: 'GET',
        json: true
    },
    channelIconOptions: {
        url: "https://slack.com/api/team.info?token=" + "xxx",
        method: 'GET',
        json: true
    },
    userListOptions: {
        url: "https://slack.com/api/users.list?&token=" + "xxx",
        method: 'GET',
        json: true,
    }
}

var flexMPP = {
    channelId: "xxx",
    oAuth: "xxx",
    webhookUrl: "https://hooks.slack.com/services/...",
    historyOptions: {
        url: "https://slack.com/api/channels.history?token=" + "xxx" + "&channel=" + "C7YR5DSQ5" + "&count=" + 20 + "&latest=#{last}",
        method: 'GET',
        json: true
    },
    channelIconOptions: {
        url: "https://slack.com/api/team.info?token=" + "xxx",
        method: 'GET',
        json: true
    },
    userListOptions: {
        url: "https://slack.com/api/users.list?&token=" + "xxx",
        method: 'GET',
        json: true,
    }
}

var slack = function(re, okCallback) {
    var request = require('request');
    request(re, function(error, response, body) {
        if (error) {
            console.log(error);
        } else {
            return okCallback(null, body);
        }
    });
};


router.get('/slackMessages', function(req, resp, next) {

    async.map([
        superMac.historyOptions,
        superMac.channelIconOptions,
        superMac.userListOptions,
        macCloud.historyOptions,
        macCloud.channelIconOptions,
        macCloud.userListOptions,
        flexManagement.historyOptions,
        flexManagement.channelIconOptions,
        flexManagement.userListOptions,
        flexMEM.historyOptions,
        flexMEM.channelIconOptions,
        flexMEM.userListOptions,
        flexMPP.historyOptions,
        flexMPP.channelIconOptions,
        flexMPP.userListOptions,
        flexSupport.historyOptions,
        flexSupport.channelIconOptions,
        flexSupport.userListOptions
    ], slack, function(err, results) {
        if (err) {
            console.log(err);
        }

        // results[0] is the response for historyOptions
        // results[1] is the response for userlistUrl
        var superMacHistory = results[0].messages;
        var superMacChannelIconInfo = results[1].team;
        var superMacUsers = results[2].members;

        var macCloudHistory = results[3].messages;
        var macCloudChannelIconInfo = results[4].team;
        var macCloudUsers = results[5].members;

        var flexManagementHistory = results[6].messages;
        var flexManagementChannelIconInfo = results[7].team;
        var flexManagementUsers = results[8].members;

        var flexMEMHistory = results[9].messages;
        var flexMEMChannelIconInfo = results[10].team;
        var flexMEMUsers = results[11].members;

        var flexMPPHistory = results[12].messages;
        var flexMPPChannelIconInfo = results[13].team;
        var flexMPPUsers = results[14].members;

        var flexSupportHistory = results[15].messages;
        var flexSupportChannelIconInfo = results[16].team;
        var flexSupportUsers = results[17].members;

        var fixedHistorySuperMac = AddUsersToHistory(superMacHistory, superMacUsers, superMacChannelIconInfo);
        var fixedHistoryMacCloud = AddUsersToHistory(macCloudHistory, macCloudUsers, macCloudChannelIconInfo);
        var fixedHistoryFlexMgmt = AddUsersToHistory(flexManagementHistory, flexManagementUsers, flexManagementChannelIconInfo);
        var fixedHistoryMEM = AddUsersToHistory(flexMEMHistory, flexMEMUsers, flexMEMChannelIconInfo);
        var fixedHistoryMPP = AddUsersToHistory(flexMPPHistory, flexMPPUsers, flexMPPChannelIconInfo);
        var fixedHistorySupport = AddUsersToHistory(flexSupportHistory, flexSupportUsers, flexSupportChannelIconInfo);

        var joinedChannelHistory = fixedHistorySuperMac.concat(fixedHistoryMacCloud).concat(fixedHistoryFlexMgmt).concat(fixedHistoryMEM).concat(fixedHistoryMPP).concat(fixedHistorySupport);

        resp.json(joinedChannelHistory);
    });
});

function DateOfPost(ts) {

    //var a = 1509465461.000262; // format för ts
    var a = ts;
    a = a.toString();
    a = a.replace(/\./g, '');
    a = a.substring(0, 13);
    a = parseInt(a);
    var fixedDate = new Date(a);

    var month = (fixedDate.getMonth() + 1).toString();
    if (month.length < 2) {
        month = "0" + month;
    }

    var day = fixedDate.getDate().toString();
    if (day.length < 2) {
        day = "0" + day;
    }

    var hour = fixedDate.getHours().toString();
    if (hour.length < 2) {
        hour = "0" + hour;
    }

    var minutes = fixedDate.getMinutes().toString();
    if (minutes.length < 2) {
        minutes = "0" + minutes;
    }

    var seconds = fixedDate.getSeconds().toString();
    if (seconds.length < 2) {
        seconds = "0" + seconds;
    }


    var dateObj = {
        year: fixedDate.getFullYear().toString(),
        month: month,
        day: day,
        hour: hour,
        minutes: minutes,
        seconds: seconds
    };
    return dateObj;
}

function AddUsersToHistory(historyList, userList, workspaceInfo) {

    var mergedList = [];
    var botProfile;

    if (userList) {
        for (var i = 0; i < userList.length; i++) {
            if (userList[i].id == "xxx") {
                botProfile = userList[i].profile;

                break;
            }
        }
    }

    if (historyList) {
        for (var i = 0; i < historyList.length; i++) {
            historyList[i].date = DateOfPost(historyList[i].ts);
            if (historyList[i].subtype == "bot_add" || historyList[i].subtype == "bot_remove" || historyList[i].subtype == "channel_join" || historyList[i].subtype == "channel_leave") {

            } else if (historyList[i].hasOwnProperty("bot_id") && historyList[i].bot_id != null) {
                if (botProfile) {

                    botProfile.image_48 = historyList[i].icons.image_64;
                }

                historyList[i].profile = botProfile;
                historyList[i].cleanUserName = historyList[i].username;

                mergedList.push(historyList[i]);
            } else if (historyList[i].hasOwnProperty("user")) {
                if (userList) {
                    for (var j = 0; j < userList.length; j++) {
                        if (historyList[i].user == userList[j].id) {
                            historyList[i].profile = userList[j].profile;
                            historyList[i].cleanUserName = historyList[i].profile.display_name;
                            mergedList.push(historyList[i]);
                        }
                    }
                }
            }
            historyList[i].workspaceIcon = workspaceInfo.icon;
            historyList[i].fromChannel = workspaceInfo.name;
        }
    }
    return mergedList;
};

module.exports = router;