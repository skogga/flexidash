// Works with Api get 

// Get dependencies
const express = require('express');
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');

var async = require('async');
// Get our API routes
const slackTemplates = require('./server/routes/slack-templates');
const slackMessages = require('./server/routes/slack-messages');

const jiraReleases = require('./server/routes/jira-releases');
const jiraPuls = require('./server/routes/jira-puls');
const jiraCCB = require('./server/routes/jira-ccb');
const numberOfPlans = require('./server/routes/number-of-plans');
const alerts = require('./server/routes/alerts');
const jiraForest = require('./server/routes/jira-forest');

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
// Cors middleware
var allowCrossDomain = function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", 'GET, POST, DELETE, PUT, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, X-Codingpedia,Authorization, Accept');
    next();
}
app.use(allowCrossDomain);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


app.use(express.static(path.join(__dirname, 'dist')));


app.use('/api', slackMessages);
app.use('/api', jiraPuls);
app.use('/api', slackTemplates);
app.use('/api', numberOfPlans);
app.use('/api', alerts);
app.use('/api', jiraReleases);
app.use('/api', jiraCCB);
app.use('/api', jiraForest);


app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'dist/index.html'));
});

const port = process.env.PORT || '3000';
app.listen(port, function() {
    console.log("Server started on port: " + port);
});