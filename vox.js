var final_transcript = '';
var recognizing = false;

const colors = [
    'black', 'white', 'green', 'blue', 'red'
]


if ('webkitSpeechRecognition' in window) {

    var recognition = new webkitSpeechRecognition();

    recognition.continuous = true;
    recognition.interimResults = true;

    recognition.onstart = function() {
        recognizing = true;
    };

    recognition.onerror = function(event) {
        console.log(event.error + ' error ');
    };

    recognition.onend = function() {
        recognizing = false;
    };

    recognition.onresult = function(event) {


        var interim_transcript = '';
        for (var i = event.resultIndex; i < event.results.length; ++i) {
            if (event.results[i].isFinal) {

                const phrase = event.results[i][0].transcript;

                const chosenColor = colors.filter(col => phrase.toLowerCase().includes(col));
                if (chosenColor.length > 0) {
                    document.getElementById("interim_span").style.color = chosenColor;
                    document.getElementById("final_span").style.color = chosenColor;
                }

                final_transcript += event.results[i][0].transcript;
            } else {
                interim_transcript += event.results[i][0].transcript;
            }
        }
        final_transcript = capitalize(final_transcript);
        final_span.innerHTML = linebreak(final_transcript);
        interim_span.innerHTML = linebreak(interim_transcript);

    };
}

var two_line = /\n\n/g;
var one_line = /\n/g;

function linebreak(s) {
    return s.replace(two_line, '<p></p>').replace(one_line, '<br>');
}

function capitalize(s) {
    return s.replace(s.substr(0, 1), function(m) {
        return m.toUpperCase();
    });
}

function startDictation(event) {
    if (recognizing) {
        recognition.stop();
        return;
    }
    final_transcript = '';
    recognition.lang = 'en-US';
    recognition.start();
    final_span.innerHTML = '';
    interim_span.innerHTML = '';
}