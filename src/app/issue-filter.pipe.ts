import { Pipe, PipeTransform } from '@angular/core';

import {FormControl} from '@angular/forms';
@Pipe({
  name: 'issueFilter',
  pure: false
})
export class IssueFilterPipe implements PipeTransform {

  transform(items: any[], chosenFilters: FormControl, allFilters: any[]): any {
 
    if (!items || !chosenFilters.value || chosenFilters.value.length === 0) {
      return items;
    }

    const resultArray = [];
    const chosenStatusFilters = [];
    const chosenPriorityFilters = [];

    const statusIndex = allFilters.findIndex(x => x.name.toLowerCase() === 'status');
    const priorityIndex = allFilters.findIndex(x => x.name.toLowerCase() === 'priority');


    for (let statuses of allFilters[statusIndex].types) {
      for (let ch of chosenFilters.value) {
        if (statuses === ch) {
          chosenStatusFilters.push(ch);
        }
      }
    }
 
    for (let prios of allFilters[priorityIndex].types) {
      for (let ch of chosenFilters.value) {
        if (prios === ch) {
          chosenPriorityFilters.push(ch);
        }
      }
    }
 
    if (chosenStatusFilters.length > 0 && chosenPriorityFilters.length === 0) {
      for (let item of items) {
        if (chosenStatusFilters.indexOf(item.status) !== -1 ) {
          resultArray.push(item);
        }
      }
    }

    else if (chosenPriorityFilters.length > 0 && chosenStatusFilters.length === 0) {
      for (let item of items) {
        if (chosenPriorityFilters.indexOf(item.priority) !== -1) {
          resultArray.push(item);
        }
      }
    }
    else if (chosenPriorityFilters.length > 0 && chosenStatusFilters.length > 0) {
      for (let item of items) {
        if (chosenPriorityFilters.indexOf(item.priority) !== -1 && chosenStatusFilters.indexOf(item.status) !== -1) {
          resultArray.push(item);
        }
      }
    }

    return resultArray;

    // return items.filter(item => item.status.indexOf(chosenFilters.value) !== -1 || item.priority.indexOf(chosenFilters.value) !== -1);
    // return items.filter(item => item.priority.indexOf(filter.value) !== -1);


  }

}
