export class SlackTemplate { 
    messageType: string;
    messageTemplate: string;
    isEditingTemplate: boolean;
}