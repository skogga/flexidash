import { Component, OnInit, Input, AfterViewInit, QueryList } from '@angular/core';

import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Http, Response, Headers, URLSearchParams, RequestOptions  } from '@angular/http';
import {BrowserModule} from '@angular/platform-browser';


import {SlackMessagesService} from '../slack-messages.service';
import { SlackTemplatesComponent} from '../slack-templates/slack-templates.component';
import { WhatsUpComponent } from '../whats-up/whats-up.component';
import { ViewChild, ViewChildren } from '@angular/core';
interface MessageResponse {
  members: JSON;
}
@Component({
  selector: 'app-slack-messages',
  templateUrl: './slack-messages.component.html',
  styleUrls: ['./slack-messages.component.css']
})
export class SlackMessagesComponent implements AfterViewInit {
  messages = [];
  userInfo = [];
  channelInfo = [];
  teamIcon = '';
  @ViewChildren('allTheseThings') things: QueryList<any>;

  constructor (private slackMessagesService: SlackMessagesService, private whatsUp: WhatsUpComponent) {}

  mouseInFeed = false;
  /* 
  ngOnInit() {
   this.getSlackMessages();

    setInterval(() => {
      this.getSlackMessages();
    }, 20000);
   } */

  ngAfterViewInit() {

    this.getSlackMessages();
    setInterval(() => {
      this.getSlackMessages();
    }, 10000);
    this.things.changes.subscribe(t => {
      this.ngForRendered();
    });

  }

  ngForRendered() {
    // console.log('NgFor is Rendered');
    this.scrollDown();
  }

  @Input()
  set ready(isReady: boolean) {
    if (isReady) {
      // this.scrollDown();
    }
  }

  finisedLoadingSlack() {
    // console.log('done loading stuff..');
  }

  trackByTs(index, mess) {
    return mess.ts;
  }

  getSlackMessages() {

    this.slackMessagesService.getSlackMessages()
        .subscribe(slackHistory => {

          if (slackHistory.length > 0) {
              const sortedHistory = slackHistory.sort(this.SortTimeStamp);
              this.whatsUp.updateTime();
              this.messages = sortedHistory;

              // console.log('Slack Messages:');
              // console.log(sortedHistory);

              if (!this.mouseInFeed) {
                this.scrollDown();
              }

          }
        },
        (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              console.log('client side error occurred - slack-messages.component');
            } else {
              console.log('server side error - slack-messages.component');
            }
        });
  }

  SortTimeStamp(a, b) {
    if (a.ts < b.ts) {
        return -1;
    }
    if (a.ts > b.ts) {
        return 1;
    }
    return 0;
  }

  ngForLog(input) {
    console.log('ngFor: ' + input);
  }


  scrollDown() {
    // console.log('Scrolling down');
    const scrollBody = document.getElementById('scrollableBodySlack');
    if (scrollBody) {
      scrollBody.scrollTop = 10000000;
    }
  }

}
