import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'countIssues',
  pure: false
})
export class CountIssuesPipe implements PipeTransform {

  transform(apps: any[]) {

    let summed = 0;

    apps.forEach(app => {
      summed += app.issues.length;
    });
    return summed;
    }

}
