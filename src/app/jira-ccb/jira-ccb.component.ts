import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Http, Response, Headers, URLSearchParams, RequestOptions  } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { JiraCcbService } from '../jira-ccb.service';
import { ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import {FormControl, FormGroup, FormBuilder, Validators} from '@angular/forms';
import { NgbPopoverModule, NgbModule  } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import axios from 'axios' // for promise examples
export interface IssueFilter {

  Status?: {
      types?: any[]
  };
  Priority?: {
      types?: any[]
  };
  Labels?: {
      types: any[]
  };
}

@Component({
  selector: 'app-jira-ccb',
  templateUrl: './jira-ccb.component.html',
  styleUrls: ['./jira-ccb.component.css'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class JiraCCBComponent implements OnInit {

  constructor(private jiraCcbService: JiraCcbService) { }
    chosenFilters: any[];
    sortingOptions = ['key', 'title', 'issuetype', 'priority', 'status', 'created', 'updated', 'reporter', 'assignee'];
    activeSort: string;

    filterControl = new FormControl();
    filters = [
        {
          name: 'Status',
          types: []
        },
        {
          name: 'Priority',
          types: []
        } 
    ];

  doneLoading = false;
  promiseNumber = 0;

  fixVersions = [
    46220,
    46221,
    46724,
    46418,
    46722,
    46725,
    46723,
    46819,
    46652,
    46655,
    46654,
    46653
  ];

  upcoming = [];

  sign = 'up';  // or minus if you want that first
  visible = false;
  toggle(event) {
    console.log('in toggle event');
      this.visible = !this.visible;
  }
  addRemoveFilter($event) {
    console.log($event);
  }

  logSorter() {
      console.log('activeSort: ' + this.activeSort);
  }
  trackByReleaseName(index, rel) {
    return rel.releaseName;
  }

  trackByIssueUpdated(index, iss) {
      return iss.updated.time;
  }
  isStatus(type) {
    return type.name === 'Status';
  }

   toggleSign($event) {
    console.log($event);

   }

   logStatusTypes (arr) {
    const filterList = {
        statuses: [],
        priorities: [],
        labels: []
    };

    for (let i = 0; i < arr.length; i++) {

        for (let j = 0; j < arr[i].project.length; j++) {

          for (let k = 0; k < arr[i].project[j].issues.length; k++) {

            if (filterList.statuses.indexOf(arr[i].project[j].issues[k].fields.status.name) !== -1) {
                const status = arr[i].project[j].issues[k].fields.status.name;
                console.log('found status: ' + status);
                filterList.statuses.push(status);
            }

            if (filterList.priorities.indexOf(arr[i].project[j].issues[k].fields.priority.name) !== -1) {
                const priority = arr[i].project[j].issues[k].fields.priority.name;
                console.log('found prio: ' + priority);
                filterList.priorities.push(priority);
            }

            for (let l = 0; l < arr[i].project[j].issues[k].fields.labels.length; l++) {
                if (filterList.labels.indexOf(arr[i].project[j].issues[k].fields.labels[l]) !== -1) {
                    const label = arr[i].project[j].issues[k].fields.labels[l];
                    console.log('found labels: ');
                    console.log(label);
                    filterList.labels.push(label);
                }
            }
          }
        }
    }


    const statusIndex = this.filters.findIndex(x => x.name.toLowerCase() === 'status');
    const priorityIndex = this.filters.findIndex(x => x.name.toLowerCase() === 'priority');

    this.filters[statusIndex].types = filterList.statuses;
    this.filters[priorityIndex].types = filterList.priorities;
}

getStatuses(arr) {

  var statuses = [];
  arr.sort(function(a,b) {return (a.fields.status.name > b.fields.status.name) ? 1 : ((b.fields.status.name > a.fields.status.name) ? -1 : 0);} );

  var current = null;
  var cnt = 0;
  for (var i = 0; i < arr.length; i++) {
      if (arr[i].fields.status.name != current) {
          if (cnt > 0) {
            statuses.push( {'name': current, 'number': cnt});
          }
          current = arr[i].fields.status.name;
          cnt = 1;
      } else {
          cnt++;
      }
  }
  if (cnt > 0) {statuses.push({'name': current,'number': cnt}); }
  return statuses;
}

  ngOnInit() {

    let getFixVersions = this.fixVersions.forEach((elem, index) => {
      var that = this;
      return Promise.resolve()
      .then(function() {
        that.getJiraFixVersion(elem).then(res => {
          that.getJiraFixVersionIssues(res.id).then(res2 => {
            res.issues = res2;
           
            res.statuses = that.getStatuses(res2);
            
            that.AddToUpcoming(res);
              
            return res;
            
          })
        });
      })
      .catch(error => {
        console.log(error);
      })
    });
 
  }

AddToUpcoming(obj):any {
  
    var nameSplit = obj.name.split(" ");
    var containerName = nameSplit[nameSplit.length - 1];
    var appName = nameSplit[0];

    let cont = this.upcoming.find(c => c.container === containerName);

    if (cont) {
      cont.apps.push( obj ); /* { appName: appName, issues: obj.iss } */
    }
    else {
      this.upcoming.push( { container: containerName, description: obj.description, apps: [ obj ] } ) /* { appName: appName, issues: obj.iss } */
    }
    console.log('Upcoming');
    console.log(this.upcoming);
   
}
  
runSerial(id) {

  var that = this;
  return Promise.resolve()
  .then(function() {
    that.getJiraFixVersion(id).then(res => {
     
      that.getJiraFixVersionIssues(res.id).then(res2 => {
        res.issues = res2;
        console.log('FixVersion w/ Desc & Issues');
        console.log(res);
        return res;
      })
    });
  })
  .catch(error => {
    console.log(error);
  })
}

  promiseAllTest() {
    this.promiseNumber += 1;
  }

  doneWithNgFor() {
    console.log('done with ngFor');
    this.doneLoading = true;
  }
 /*  
 trackByFn(index, upcoming) {
    return upcoming.project.length; // or item.id
  } 
  */

  getJiraFixVersion(id): any {
    
    return new Promise((resolve, reject) => {
      this.jiraCcbService.getJiraFixVersion(id).subscribe(fixVersion => {
        resolve( fixVersion );
        },
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            console.log('client side error occurred (jira fix versions)');
          } else {
            console.log('server side error (jira fix versions)');
          }
        }
      );
    })
  }

   getJiraFixVersionIssues(id): any {
    return new Promise((resolve, reject) => {
      this.jiraCcbService.getJiraFixVersionIssues(id).subscribe(jiraFixVersionIssues => {
        resolve(jiraFixVersionIssues);
        },
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            console.log('client side error occurred (jira fix versions)');
          } else {
            console.log('server side error (jira fix versions)');
          }
        }
      );
    })
  }


/* 
  getJiraCCBSpecific(project, id, collection) {

    this.jiraCcbService.getJiraCCBSpecific(project, id).subscribe(jiraCCB => {

      const containerIndex = this.upcomingIssues.findIndex(x => x.containerName === collection);
      const projectIndex = this.upcomingIssues[containerIndex].project.findIndex(x => x.projectName === project);

      const statusIndex = this.filters.findIndex(x => x.name.toLowerCase() === 'status');
      const priorityIndex = this.filters.findIndex(x => x.name.toLowerCase() === 'priority');

      
      jiraCCB.forEach(element => {
        const priority = element.fields.priority.name;
        const status = element.fields.status.name;
        if (!this.filters[priorityIndex].types.includes(priority)) {
          this.filters[priorityIndex].types.push(priority);
        }
        if (!this.filters[statusIndex].types.includes(status)) {
          this.filters[statusIndex].types.push(status);
        }
      });
    
      this.upcomingIssues[containerIndex].project[projectIndex].issues = jiraCCB;
      },
      (err: HttpErrorResponse) => {
        if (err.error instanceof Error) {
          console.log('client side error occurred (upcoming issues)');
      } else {
          console.log('server side error (upcoming issues)');
      }
      });
  } */

 
}
