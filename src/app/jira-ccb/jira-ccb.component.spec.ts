import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JiraCCBComponent } from './jira-ccb.component';

describe('JiraCCBComponent', () => {
  let component: JiraCCBComponent;
  let fixture: ComponentFixture<JiraCCBComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JiraCCBComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JiraCCBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
