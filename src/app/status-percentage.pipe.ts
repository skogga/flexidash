import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'statusPercentage',
  pure: false
})
export class StatusPercentagePipe implements PipeTransform {

  transform(number: any, app: any) {
    let statuses = app.statuses;
    let allStatuses = 0;

    statuses.forEach(elem => {
      allStatuses += elem.number;
    })
    
    return Math.floor(number / allStatuses);
  }


}
