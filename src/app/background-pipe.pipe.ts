import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'backgroundPipe'
})
export class BackgroundPipePipe implements PipeTransform {

  transform(value: string, cutoffPoint: string): any { /* , maxLength: number */
    if (!value) {
        return value;
    }

    let newText = value.substring(0, value.indexOf('*' + cutoffPoint + '*'));


    return newText;

  }

}
