import { Component, Input, OnInit } from '@angular/core';
declare var $: any;
import { NgbPopoverModule, NgbModule  } from '@ng-bootstrap/ng-bootstrap';
import {FormsModule} from '@angular/forms';
@Component({
  selector: 'app-forest-item',
  templateUrl: './forest-item.component.html',
  styleUrls: ['./forest-item.component.css']
})
export class ForestItemComponent implements OnInit {

  constructor() { }

  @Input()
  tree;

  ngOnInit() {
    this.accordionSetup(); 
  }

  toggleToolTipInfo(inEvent, item) {
    $(inEvent.target).tooltip({
        title: "<h1><strong>HTML</strong> inside <code>the</code> <em>tooltip</em></h1>",
        html: true,
        placement: "left",
        disabled: true,
        close: function( event, ui ) {
            $(this).tooltip('disable');
        }
    });

    $(inEvent.target).on('click', function () {
        $(this).tooltip('enable').tooltip('open');
    });

  }

  toggleToolTip(header, body) {
    $('.htmlTooltip').tooltip({
      title: '<label style="font-size:1.5em;"><em style="font-size:1.5em;">' + header + '</em></label><aside style=" background-color:white; padding:1em;"><label style="font-size:1.4em;color:black;">' + body + '</label></aside>', html: true, placement: "top"});
  }


  accordionSetup() {

    $('.accordion h1, h2, h3, h4, h5, h6').unbind().click(function(e) {
        const target = e.target,
            name = target.nodeName.toUpperCase();

        const kids = target.parentNode.childNodes;
        console.log();
        if ($(target).hasClass('open')) {
            console.log('had open');
            $(target).removeClass('open');
            $(target).parent().parent().next().children('.holder').removeClass('open'); 

           if ($(target).parent().next('.nokids')) {
                $(target).parent().next('.nokids').removeClass('open');
            }
        } else {
            console.log('did not have open');
            $(target).addClass('open');
            $(target).parent().parent().    next().children('.holder').addClass('open');

            if ($(target).parent().next('.nokids')) {
                $(target).parent().next('.nokids').addClass('open');
            }
            console.log($(target).next().children().has('.holder').length);
        }
    });
  }

}
