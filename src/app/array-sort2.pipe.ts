import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'arraySort2'
})

export class ArraySort2Pipe implements PipeTransform {


  transform(array: any[], field: string): any[] {
    array.sort((a: any, b: any) => {

      if (field == 'key') {
        let elemOne = a[field];
        let elemTwo = b[field];
        if (elemOne < elemTwo) {
          return -1;
        } else if (elemOne > elemTwo) {
          return 1;
        } else {
          return 0;
        }
      }
      else if ((a.fields[field] && b.fields[field])) {

      if (a.fields[field].hasOwnProperty('displayName') || b.fields[field].hasOwnProperty('displayName')) {
        let elemOne = a.fields[field].displayName || 'z';
        let elemTwo = b.fields[field].displayName || 'z';
        if (elemOne < elemTwo ) {
          return -1;
        } else if (elemOne > elemTwo ) {
          return 1;
        } else {
          return 0;
        }
      }
      else if (a.fields[field].hasOwnProperty('name') || b.fields[field].hasOwnProperty('name')) {
        let elemOne = a.fields[field].name || 'z';
        let elemTwo = b.fields[field].name || 'z';
        if (elemOne < elemTwo ) {
          return -1;
        } else if (elemOne > elemTwo ) {
          return 1;
        } else {
          return 0;
        }
      }
    }

    });
    return array;
  }

}
