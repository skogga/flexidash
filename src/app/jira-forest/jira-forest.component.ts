import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Http, Response, Headers, URLSearchParams, RequestOptions  } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { JiraForestService } from '../jira-forest.service';
import { ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { ForestItemComponent } from '../forest-item/forest-item.component';




interface Tree {
  name: string;
}

@Component({
  selector: 'app-jira-forest',
  templateUrl: './jira-forest.component.html',
  styleUrls: ['./jira-forest.component.css']
})
export class JiraForestComponent implements OnInit {
  trees: Observable<Tree[]>;

  /* forest = [
    {levelId: 0, name: '0.1', children: [
        {levelId: 1, name: '1.1', children: []},
        {levelId: 1, name: '1.2', children: [
          {levelId: 2, name: '2.1', children: [
            {levelId: 3, name: '3.1', children: []},
            {levelId: 3, name: '3.2', children: [
              {levelId: 4, name: '4.1', children: [
                {levelId: 5, name: '5.1', children: [
                  {levelId: 6, name: '6.1', children: [
                    {levelId: 7, name: '7.1', children: [
                      {levelId: 8, name: '8.1', children: []}
                    ]}
                  ]}
                ]}
              ]}
            ]}
          ]}
        ]}
      ]
    },
    {levelId: 0, name: '0.2', children: [
      {levelId: 1, name: '1.1', children: []}
    ]},
    {levelId: 0, name: '0.3', children: []}
  ]; */


  constructor(private jiraForestService: JiraForestService, private forestItem: ForestItemComponent) { }
  forest: any;
  ngOnInit() {
    this.trees = Observable.of(this.forest);
    this.getJiraForest();
 
     // this.getJiraFormulaInfo();
  }
  trackById(index, tree) {
    return tree.name;
  }

  
  getJiraForest() {

    this.jiraForestService.getJiraForest().subscribe(jiraForest => {
      console.log('jira Forest');
      console.log(jiraForest);
      this.forest = jiraForest;
      },
      (err: HttpErrorResponse) => {
        if (err.error instanceof Error) {
          console.log('client side error occurred getJiraForest');
      } else {
          console.log('server side error getJiraForest');
          console.log(err);
      }
      });
  }
  getJiraFormulaInfo() {

    this.jiraForestService.getJiraFormulaInfo().subscribe(formulaInfo => {
      console.log(formulaInfo);
      },
      (err: HttpErrorResponse) => {
        if (err.error instanceof Error) {
          console.log('client side error occurred (upcoming issues)');
      } else {
          console.log('server side error (upcoming issues)');
      }
      });
  }
}
