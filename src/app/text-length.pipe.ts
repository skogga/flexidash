import { Pipe, PipeTransform } from '@angular/core';
/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
*/
@Pipe({name: 'textLength'})
export class TextLengthPipe implements PipeTransform {
  transform(value: string, maxLength: number): any {
    if (!value) {
        return value;
    }

    let newText = value;

    if (value.length > maxLength) {
        newText = value.substring(0, (maxLength - 3));
        newText += '...';
    }

    return newText;

  }
}
