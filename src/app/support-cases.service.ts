import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class SupportCaseService {

    constructor(private http: Http) { }
    
    getSupportCases() {
        return this.http.get('http://localhost:3000/api/supportCases')
        .map(res => res.json());
    }

    updateSupportNumber(supportCase) {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.put('http://localhost:3000/api/supportCase/' + supportCase._id, JSON.stringify(supportCase), {headers: headers})
            .map(res => res.json());
    }
}
/* 
RD0049523
sesoco3319
localhost:3000
*/