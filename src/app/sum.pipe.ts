import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sum',
  pure: false
})
export class SumPipe implements PipeTransform {

  transform(items: any[], attr: string): any {
   /* console.log('items');
    console.log(items);
    console.log(items.length); */

    let keyArr: any[] = Object.keys(items), summed = 0;

    // loop through the object,
    // pushing values to the return array
    keyArr.forEach((key: any) => {

      summed += items[key].issues.length;
      // dataArr.push(items[key].issues);
    });
    // return dataArr.length;
    return summed;

  }

}
