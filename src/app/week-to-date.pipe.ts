import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'weekToDate'})
export class WeekToDatePipe implements PipeTransform {

  transform(value: string): any {
  
  if (!value) {
      return value;
  }

  let inputDate = value.split(".");

  var y = parseInt(inputDate[0]);
  var w = parseInt(inputDate[1]);
  var day = parseInt(inputDate[2]);
  var dat = (1 + (w - 1) * 7); // 1st of January + 7 days for each week

  var dateVersion = new Date(y, 0, dat + day);
  var year = (dateVersion.getFullYear()).toString();
  var month = (dateVersion.getMonth() + 1).toString();
  if (month.length < 2){
    month = "0" + month;
  }
  var date = dateVersion.getDate().toString();
  if (date.length < 2){
    date = "0" + date;
  }


  
  return (year + "-" + month + "-" + date);
  
  }

}




    

 


