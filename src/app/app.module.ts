import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import {HttpClientModule, HttpClient} from '@angular/common/http';

import { AppComponent } from './app.component';
import { SlackMessagesComponent } from './slack-messages/slack-messages.component';
import { SlackTemplatesComponent } from './slack-templates/slack-templates.component';
import { SupportCasesComponent } from './support-cases/support-cases.component';


import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TeamIconDirective } from './team-icon.directive';
import { JiraReleaseComponent } from './jira-release/jira-release.component';
import {CustExtBrowserXhr} from './cust-ext-browser-xhr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsModalModule } from 'ng2-bs3-modal';
import { TextLengthPipe } from './text-length.pipe';


import { NumberOfPlansComponent } from './number-of-plans/number-of-plans.component';
import { WhatsUpComponent } from './whats-up/whats-up.component';
import { JiraPulsComponent } from './jira-puls/jira-puls.component';
import { PieChartComponent } from './pie-chart/pie-chart.component';
import { NgxChartsModule} from '@swimlane/ngx-charts';
import { GaugeChartComponent } from './gauge-chart/gauge-chart.component';
import { WeekToDatePipe } from './week-to-date.pipe';

import { HeaderComponent } from './header/header.component';
import { AlertComponent } from './alert/alert.component';
import { DatePickerComponent } from './date-picker/date-picker.component';

/* Material Design */
import { MatFormFieldModule } from '@angular/material';
import { MatInputModule, MatNativeDateModule } from '@angular/material';
import {  MatDatepickerModule } from '@angular/material/datepicker';
import {MatSelectModule} from '@angular/material/select';

import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatSnackBar} from '@angular/material';
import { MatRadioModule } from '@angular/material/radio';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';


import { DateFormatterPipe } from './date-formatter.pipe';
import { JiraReleaseService } from '../../server/routes/app/jira-release.service';
import { EvenoddPipe } from './evenodd.pipe';
import { ArraySortPipe } from './array-sort.pipe';
import { JiraCCBComponent } from './jira-ccb/jira-ccb.component';
import { SumPipe } from './sum.pipe';
import { IssueFilterPipe } from './issue-filter.pipe';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { LastElementDirective } from './last-element.directive';
import { OrderByPipe } from './order-by.pipe';
import { JiraForestComponent } from './jira-forest/jira-forest.component';
import { ForestItemComponent } from './forest-item/forest-item.component';
import { IssueFilter2Pipe } from './issue-filter2.pipe';
import { ArraySort2Pipe } from './array-sort2.pipe';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { CountIssuesPipe } from './count-issues.pipe';
import { FirstWordPipe } from './first-word.pipe';
import { StatusPercentagePipe } from './status-percentage.pipe';
import { TestComponent } from './test/test.component';
import { BackgroundPipePipe } from './background-pipe.pipe';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};
@NgModule({
  declarations: [
    AppComponent,
    SlackMessagesComponent,
    SlackTemplatesComponent,
    SupportCasesComponent,
    TeamIconDirective,
    JiraReleaseComponent,
    TextLengthPipe,
    NumberOfPlansComponent,
    WhatsUpComponent,
    JiraPulsComponent,
    PieChartComponent,
    GaugeChartComponent,
    WeekToDatePipe,
    HeaderComponent,
    AlertComponent,
    DatePickerComponent,
    DateFormatterPipe,
    EvenoddPipe,
    ArraySortPipe,
    JiraCCBComponent,
    SumPipe,
    IssueFilterPipe,
    PortfolioComponent,
    LastElementDirective,
    OrderByPipe,
    JiraForestComponent,
    ForestItemComponent,
    IssueFilter2Pipe,
    ArraySort2Pipe,
    CountIssuesPipe,
    FirstWordPipe,
    StatusPercentagePipe,
    TestComponent,
    BackgroundPipePipe
  ],
  imports: [
    MatFormFieldModule,
    MatInputModule,
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    BsModalModule,
    NgxChartsModule,
    MatDatepickerModule,
    MatSelectModule,
    HttpClientModule,
    MatNativeDateModule,
    MatRadioModule,
    MatSnackBarModule,
    NgbModule.forRoot(),
    PerfectScrollbarModule
  ],
  exports: [JiraReleaseComponent],

  providers: [DatePickerComponent, JiraReleaseService, ForestItemComponent, JiraReleaseComponent, MatSnackBarModule, MatSnackBar, {
    provide: PERFECT_SCROLLBAR_CONFIG,
    useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
  } ],
  bootstrap: [AppComponent],

})
export class AppModule { }
