import { TestBed, inject } from '@angular/core/testing';

import { NumberOfPlansService } from './number-of-plans.service';

describe('NumberOfPlansService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NumberOfPlansService]
    });
  });

  it('should be created', inject([NumberOfPlansService], (service: NumberOfPlansService) => {
    expect(service).toBeTruthy();
  }));
});
