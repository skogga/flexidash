import { TestBed, inject } from '@angular/core/testing';

import { JiraForestService } from './jira-forest.service';

describe('JiraForestService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JiraForestService]
    });
  });

  it('should be created', inject([JiraForestService], (service: JiraForestService) => {
    expect(service).toBeTruthy();
  }));
});
