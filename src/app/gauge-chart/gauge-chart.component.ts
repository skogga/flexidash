import {Component, OnInit, Input, NgModule, OnDestroy , AfterViewInit} from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import * as d3 from 'd3';

import { JiraPulsService } from '../jira-puls.service';
import { JiraReleaseService } from '../jira-release.service';
import { Subscription } from 'rxjs/Subscription';

import { select } from 'd3-selection';
import { arc } from 'd3-shape';

declare var $: any;

@Component({
  selector: 'app-gauge-chart',
  templateUrl: './gauge-chart.component.html',
  styleUrls: ['./gauge-chart.component.css']
})
export class GaugeChartComponent implements OnDestroy {


  activeChartIsPuls = false;

  chartInfoReleasesValueItem: any;
  chartInfoPulsesValueItem: any;
  subscriptionJiraPulsValueItem: Subscription;
  subscriptionJiraReleasesValueItem: Subscription;

  macColor = 'rgb(242, 66, 54)';
  mppColor = 'rgb(55, 80, 92)';
  memColor = 'rgb(68, 168, 120)';
  macCloudColor =  'rgb(46, 134, 171)';
  recommendedColor = 'rgb(246, 245, 174)';
  secondaryColor = 'rgb(86, 85, 84)';
  colorScheme = {
    domain: [
      this.macColor,
      this.mppColor,
      this.memColor,
      this.macCloudColor,
      this.recommendedColor,
      this.secondaryColor
    ]
  };

  currentGaugeData: any;

  gaugeMaxVal = 100;
  totalMAC = 1;
  totalMPP = 1;
  totalMEM = 1;

  macValueItems = 0;
  mppValueItems = 0;
  memValueItems = 0;

  data = [
    {
      'name': 'MAC',
      'value': 40
    },
    {
      'name': 'MPP',
      'value': 40
    },
    {
      'name': 'MEM',
      'value': 40
    },
    {
      'name': 'MacCloud',
      'value': 40
    },
    {
      'name': 'Recommended',
      'value': 40
    }
  ];
  constructor(private jiraReleaseService: JiraReleaseService, private jiraPulsService: JiraPulsService) {

          this.subscriptionJiraReleasesValueItem = this.jiraReleaseService.getGaugeChartInfo().subscribe(chartInfo => {

            this.chartInfoReleasesValueItem = chartInfo;
            this.currentGaugeData = chartInfo;
            this.updateChart('releases'); 

          });
          this.subscriptionJiraPulsValueItem = this.jiraPulsService.getGaugeChartInfo().subscribe(chartInfo => {

            this.chartInfoPulsesValueItem = chartInfo;

          });
      }

  ngAfterViewInit() {

    $('app-gauge-chart text').remove();
    const tr = $('g.gauge.chart').attr('transform');
    $('g.gauge.chart').attr({'transform': tr + ' scale(0.98, 0.98)' + ' translate(0, 10)'});

  }



  updateChart(chartInput) {

    if (chartInput === 'releases') {

      this.currentGaugeData = this.chartInfoReleasesValueItem;
      this.data = this.addToGauge(this.chartInfoReleasesValueItem);
    }
    else if (chartInput === 'pulses') {

      this.currentGaugeData = this.chartInfoPulsesValueItem;
      this.data = this.addToGauge(this.chartInfoPulsesValueItem);
    }
  }

  addToGauge(array) {
    const arr = [];
    for (let i = 0; i < array.length; i++) {
      let val = Math.round(array[i].epics / array[i].total * 100);
      if (!val) {
        val = 0;
      }
      const obj = {
        'name': array[i].name,
        'value': val
      };
      arr.push(obj);
    }

    return arr;
  }


  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.subscriptionJiraPulsValueItem.unsubscribe();
    this.subscriptionJiraReleasesValueItem.unsubscribe();
  }


  onSelect(event) {
    console.log(event);
  }

}
