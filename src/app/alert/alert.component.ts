import { Component, OnInit, Input  } from '@angular/core';
import { AlertsService } from '../alerts.service';
import { HttpClient } from '@angular/common/http';
import { Http, Response } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';

import { MatFormFieldModule } from '@angular/material';
import { MatInputModule, MatRadioChange } from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { DatePickerComponent } from '../date-picker/date-picker.component';

declare var $: any;
import {MatDatepickerInputEvent} from '@angular/material/datepicker';



import {
  ReactiveFormsModule,
  FormsModule,
  FormGroup,
  FormControl,
  Validators,
  FormBuilder
} from '@angular/forms';

import {MatRadioModule} from '@angular/material/radio';

interface Alert {
  publishDate: Date;
  expiryDate: Date;
  title: string;
  message: string;
  priority: string;
}
@Component({

  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {

  constructor(
    private alertsService: AlertsService,
    private datePicker: DatePickerComponent) {
     }


  publishDate: any;
  expiryDate: any;
  priority: string;
  title: string;
  message: string;


  alerts: any;
  activeAlert: any;

  alert: Alert;
  /*
  alertTitle: string;
  alertMessage: string;
  alertPriority: any;
  alertPublishDate: any;
  alertExpiryDate: any;
  */
  isEditingAlert = false;

  ngOnInit() {
    // this.alertPriority = 0;
    this.getAlerts();

    setInterval(() => {
      if (!this.isEditingAlert) {
      $('#alertTextElement').fadeOut(500);
      $('#alertTextExpiry').fadeOut(500);

      setTimeout(() => {
        this.changeActiveAlert();
      }, 500);
      this.getAlerts();
   }}, 5000);
  }


  changeActiveAlert() {
    let indexOfCurrent = 0;

    if (this.alerts) {
      for (let i = 0; i < this.alerts.length; i++) {
        if (this.activeAlert._id === this.alerts[i]._id) {
          indexOfCurrent = i;
        }
      }

      this.activeAlert = this.alerts[(indexOfCurrent + 1) % this.alerts.length];

      $('#alertTextElement').fadeIn(500);
      $('#alertTextExpiry').fadeIn(500);

    }

      // will be called when the element finishes fading out
      // if selector matches multiple elements it will be called once for each
  }
  radioChange(event: MatRadioChange) { }

  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  arraySort(a, b) {
    if (a._id < b._id) {
      return -1;
    }
    if (a._id > b._id) {
      return 1;
    }
    return 0;
  }

  getAlerts() {
    this.alertsService.getAlerts()
    .subscribe(alerts => {

      const alertsWithinTimeFrame = [];
      const currentDate = new Date();

      for (let i = 0; i < alerts.length; i++) {

          if (currentDate < new Date(alerts[i].expiryDate) && currentDate > new Date(alerts[i].publishDate)) {
            alertsWithinTimeFrame.push(alerts[i]);
          }
        }
        if (alertsWithinTimeFrame.length < 1) {
          const mess = {
            message: 'No Alerts within timeframe',
            priority: 0
          };
          this.activeAlert = mess;
          this.alerts = mess;
        }

        else {
          if (alertsWithinTimeFrame.length > 0) {
            alertsWithinTimeFrame.sort(this.arraySort);
          }
        if (!this.alerts) {
          this.alerts = alertsWithinTimeFrame;
          this.activeAlert = this.alerts[0];
          return;
        }
        else {
          let sameArr = true;
          if (this.alerts.length > 0) {
            this.alerts.sort(this.arraySort);
          }
          for (let i = 0; i < this.alerts.length; i++) {
            for (let j = 0; j < alertsWithinTimeFrame.length; j++) {
              if (this.alerts[i]._id !== alertsWithinTimeFrame[j]._id) {
                sameArr = false;
              }
            }
          }
          if (!sameArr) {
            this.alerts = alertsWithinTimeFrame;
          }
        }
      }

    });
  }

  resetForm() {
    this.publishDate = '';
    this.expiryDate = '';
    this.title = '';
    this.message = '';
    this.priority = '';
  }
  setAlertToActive() {
    this.publishDate = this.activeAlert.publishDate;
    this.expiryDate = this.activeAlert.expiryDate;
    this.title = this.activeAlert.title;
    this.message = this.activeAlert.message;
    this.priority = this.activeAlert.priority;
  }

  dateInput(dateType, date) {
    if (dateType === 'publishDate') {
      this.publishDate = date;
    }
    else if (dateType === 'expiryDate') {
      this.expiryDate = date;
    }
  }
  setPriority(prio) {
    this.priority = prio;
  }
  logActive() { }

  postAlert() {
    const newAlert = {
      publishDate: this.publishDate,
      expiryDate: this.expiryDate,
      title:  this.title,
      message: this.message,
      priority: this.priority
    };
    console.log('Posting new alert:');
    console.log(newAlert);
    this.alertsService.postAlert(newAlert);
  }

  postAlertChange() {

    const editedAlert = {
      _id: this.activeAlert._id,
      publishDate: this.publishDate,
      expiryDate: this.expiryDate,
      title:  this.title,
      message: this.message,
      priority: this.priority
    };



    this.alertsService.updateAlert(editedAlert).subscribe(data => {
        this.publishDate = editedAlert.publishDate;
        this.expiryDate = editedAlert.expiryDate;
        this.title = editedAlert.title;
        this.message = editedAlert.message;
        this.priority = editedAlert.priority;
    });


    this.alertsService.getAlerts();
  }


  removeAlert() {

    const editedAlert = {
      _id: this.activeAlert._id,
      publishDate: this.publishDate,
      expiryDate: this.expiryDate,
      title:  this.title,
      message: this.message,
      priority: this.priority
    };


    this.alertsService.removeAlert(editedAlert).subscribe(data => {
        console.log('Removed alert');
    });
  }



}

/*
db.createColleciton("Alerts");

db.Alerts.insert({
  message: 'asdf',
  creationDate: '2017-11-14',
  endDate: '2017-11-15'
});
*/
