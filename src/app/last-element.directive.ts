import { Directive, ElementRef, Input, Output, EventEmitter } from '@angular/core';

@Directive({
  selector: '[appLastElement]'
})
export class LastElementDirective {

  @Input() appLastElement: any;
  @Output() lastFunc = new EventEmitter();
  constructor(private el: ElementRef) {
    this.setTimer(); // somehow lastElem value is available on next tick
  }
  setTimer() {
    setTimeout(() => {
      if (this.appLastElement) {
        this.lastFunc.emit();
      }
    }, 0);
  }
}
