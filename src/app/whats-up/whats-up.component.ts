import { Component, OnInit } from '@angular/core';
import { JiraReleaseService } from '../jira-release.service';
import { JiraReleaseComponent } from '../jira-release/jira-release.component';

@Component({
  selector: 'app-whats-up',
  templateUrl: './whats-up.component.html',
  styleUrls: ['./whats-up.component.css'],
  // providers: [JiraReleaseService] -- fuckade upp om låg här (blev tomt i subscription i templates component)
})
export class WhatsUpComponent implements OnInit {

    constructor() { }
    isPostingToSlack: boolean;

    slackFeedRequestTime: Date; // this.formatDate(new Date());

    updateTime() {
        // console.log('in update time in whats up');
        this.slackFeedRequestTime = new Date(); // this.formatDate(new Date());
    }
    ngOnInit() {
        this.isPostingToSlack = false;
    }

}
