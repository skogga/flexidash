import { Component, OnInit } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Http, Response } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';

import { MatFormFieldModule } from '@angular/material';
import { MatInputModule, MatRadioChange } from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { DatePickerComponent } from '../date-picker/date-picker.component';

declare var $: any;
import {MatDatepickerInputEvent} from '@angular/material/datepicker';

import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatSnackBar} from '@angular/material';



import {
  ReactiveFormsModule,
  FormsModule,
  FormGroup,
  FormControl,
  Validators,
  FormBuilder
} from '@angular/forms';



@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class HeaderComponent implements OnInit {


  constructor(public snackBar: MatSnackBar) { }

  currentDate = this.formatDate(new Date());

  ngOnInit() {
    setInterval(() => {
      this.updateTime();
    }, 1000);

  }
  openSnackBar() {
    /* this.snackBar.openFromComponent(PizzaPartyComponent, {
      duration: 500,
    }); */

    this.snackBar.open(`Snackbar Test`, 'Undo', {
        duration: 4000,
      });
  }

  updateTime() {
    this.currentDate = this.formatDate(new Date());
  }

    formatDate(d) {

        const year = d.getFullYear().toString();
        let month = (d.getMonth() + 1).toString();
        if (month.length < 2) {
            month = '0' + month;
        }
        let date = d.getDate().toString();
        if (date.length < 2) {
            date = '0' + date;
        }

        let hour = d.getHours().toString();
        if (hour.length < 2) {
        hour = '0' + hour;
        }
        let minutes = d.getMinutes().toString();
        if (minutes.length < 2) {
            minutes = '0' + minutes;
        }

        let seconds = d.getSeconds().toString();
        if (seconds.length < 2) {
            seconds = '0' + seconds;
        }

        const ts = {year: year,
                month: month,
                date: date,
                time: hour + ':' + minutes + ':' + seconds,
                week: this.getWeekNumber(new Date()),
                dayNumber: new Date().getDay()
            };
        return ts;
    }

    getWeekNumber(d: Date): number {
        // Copy date so don't modify original
        d = new Date(+d);
        d.setHours(0, 0, 0);
        // Set to nearest Thursday: current date + 4 - current day number
        // Make Sunday's day number 7
        d.setDate(d.getDate() + 4 - (d.getDay() || 7));
        // Get first day of year
        const yearStart = new Date(d.getFullYear(), 0, 1);
        // Calculate full weeks to nearest Thursday
        const weekNo = Math.ceil((((d.valueOf() - yearStart.valueOf()) / 86400000) + 1) / 7);
        // Return array of year and week number
        return weekNo;
    }

}
/*

@Component({
    selector: 'app-snack-bar-component-example-snack',
    template: `test test`,
    styles: [`.example-pizza-party { color: hotpink; }`],
  })
export class PizzaPartyComponent {}
*/

