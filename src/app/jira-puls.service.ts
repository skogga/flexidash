import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptionsArgs } from '@angular/http';
import 'rxjs/add/operator/map';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class JiraPulsService {

    constructor(private http: Http) { }

    private subjectPie = new Subject<any>();
    private subjectGauge = new Subject<any>();

    sendPieChartInfo(chartInfo: any) {
        this.subjectPie.next(chartInfo);
    }

    clearPieChartInfo() {
        this.subjectPie.next();
    }

    getPieChartInfo(): Observable<any> {
        return this.subjectPie.asObservable();
    }

    sendGaugeChartInfo(chartInfo: any) {
        this.subjectGauge.next(chartInfo);
    }

    clearGaugeChartInfo() {
        this.subjectGauge.next();
    }

    getGaugeChartInfo(): Observable<any> {
        return this.subjectGauge.asObservable();
    }

    getJiraPuls() {
        return this.http.get('http://localhost:3000/api/jiraPuls')
            .map(res => res.json());
    }
}
/*
localhost:3000
RD0049523
sesoco3319
*/

