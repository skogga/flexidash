import { TestBed, inject } from '@angular/core/testing';

import { SupportCasesService } from './support-cases.service';

describe('SupportServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SupportCasesService]
    });
  });

  it('should be created', inject([SupportCasesService], (service: SupportCasesService) => {
    expect(service).toBeTruthy();
  }));
});
