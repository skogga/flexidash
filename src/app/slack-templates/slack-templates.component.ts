

import { Component, OnInit, Input, NgModule, OnDestroy , AfterViewInit  } from '@angular/core';

import { SlackTemplatesService } from '../slack-templates.service';
import { SlackTemplate } from '../../SlackTemplate';
import { HttpClient } from '@angular/common/http';
import { Http, Response, Headers, RequestOptionsArgs } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';

import { JiraReleaseService } from '../jira-release.service';
import { JiraReleaseComponent } from '../jira-release/jira-release.component';
import { WhatsUpComponent } from '../whats-up/whats-up.component';

import { Subscription } from 'rxjs/Subscription';
/* Material Design */
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule } from '@angular/material';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
@Component({
    moduleId: module.id,
    selector: 'app-slack-templates',
    templateUrl: './slack-templates.component.html',
    styleUrls: ['./slack-templates.component.css'],
    providers: [JiraReleaseComponent] // innan component här gav errors No provider for service // JiraReleaseService
})
export class SlackTemplatesComponent implements OnDestroy {
    issueSelected = true;
    selectedValue: string;
    selectedRelease: any;

    goMessageIssues: [{
        key: string,
        title: string,
        url: string,
        inRelease: boolean
    }];


    goIssues: any;
    subscriptionGoIssues: Subscription;

    slackTemplates: SlackTemplate[];
    messageType: string;
    messageTemplate: string;
    isEditingTemplate: boolean;

    isPostingGoNoGoMessage = false;
    isPostingAbscenceMessage = false;
    isPostingWorkFromHomeMessage = false;
    isPostingCustomMessage = false;
    isPostingToSlack = false;
/*
    isEditingGoTemplate = false;
    isEditingNoGoTemplate = false; */
    goTemplate: SlackTemplate;
    noGoTemplate: SlackTemplate;
    abscenceTemplate: SlackTemplate;
    workFromHomeTemplate: SlackTemplate;


    // build prod stuff
    editingAbscenceTemplate = false;
    editingGoTemplate = false;
    editingWorkFromHomeTemplate = false;
    editingNoGoTemplate = false;
    // build prod stuff

    postToChannel = {
        MAC: false,
        Cleod: false,
        MPP: false,
        MEM: false,
        Support: false,
        Management: false
    };


    @Input()  name: string;
    @Input()  noGoInput: string;
    @Input()  goInput: string;
    @Input()  customMessageInput: string;
    @Input()  workFromHomeInput: string;
    @Input()  abscenceInput: string;

    constructor(
        private slackTemplateService: SlackTemplatesService,
        private releaseComponent: JiraReleaseComponent,
        private jiraReleaseService: JiraReleaseService,
        private http: Http

        ) {


              this.subscriptionGoIssues = this.jiraReleaseService.getGoIssues().subscribe((data: any) => {
                // console.log('go issues (getGoIssues):');
                // console.log(data);

                this.goIssues = data;
              }, (error: any) => {
                if (typeof error['Errors'] != 'undefined') {
                    console.log('error in getGoIssues');
                    console.log(error['Errors']);
                }
                });

        this.isEditingTemplate = false;
    }


    ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        this.subscriptionGoIssues.unsubscribe();
    }

    logStuff() {
        console.log('post to:');
        console.log(this.postToChannel);
    }


    submitGoIssues(iss) {
        const issues = {
            MAC: [],
            MACCLOUD: [],
            MPP: [],
            MEM: []
        };

        let activeRelease: any;
        for (let i = 0; i < this.goIssues.length; i++) {
            if (this.goIssues[i].releaseName === iss) {
                activeRelease = this.goIssues[i];
            }
        }

        if (activeRelease) {
            if (activeRelease.hasOwnProperty('MACissues')) {
                for (let i = 0; i < activeRelease.MACissues.length; i++) {
                    if (activeRelease.MACissues[i].isInGoRelease) {
                        issues.MAC.push(activeRelease.MACissues[i]);
                    }
                }
            }
            if (activeRelease.hasOwnProperty('MPPissues')) {
                for (let i = 0; i < activeRelease.MPPissues.length; i++) {
                    if (activeRelease.MPPissues[i].isInGoRelease) {
                        issues.MPP.push(activeRelease.MPPissues[i]);
                    }
                }
            }

            if (activeRelease.hasOwnProperty('MEMissues')) {
                for (let i = 0; i < activeRelease.MEMissues.length; i++) {
                    if (activeRelease.MEMissues[i].isInGoRelease) {
                        issues.MEM.push(activeRelease.MEMissues[i]);
                    }
                }
            }
        }
        this.sendToSlack(this.goTemplate.messageTemplate, issues);

    }



    getSlackTemplates() {
        this.slackTemplateService.getSlackTemplates()
        .subscribe(slackTemplates => {
            this.slackTemplates = slackTemplates;

            for (let i = 0; i < slackTemplates.length; i++) {
                const messageType = slackTemplates[i].messageType;
                // console.log('Template: ');
                // console.log(slackTemplates[i]);

                if (messageType === 'goMessage') {
                    this.goTemplate = slackTemplates[i]; // .messageTemplate;
                }else if (messageType === 'noGoMessage') {
                    this.noGoTemplate = slackTemplates[i];
                }else if (messageType === 'abscenceMessage') {
                    this.abscenceTemplate = slackTemplates[i];
                }else if (messageType === 'workFromHomeMessage') {
                    this.workFromHomeTemplate = slackTemplates[i];
                }
            }
        });
    }

    ngOnInit() {
        this.getSlackTemplates();
    }

    updateMessageTemplate(slackTemplate, newMessage) {
        const _slackTemplate = {
            _id: slackTemplate._id,
            messageType: slackTemplate.messageType,
            messageTemplate: newMessage // slackTemplate.messageTemplate
        };
        this.slackTemplateService.updateMessageTemplate(_slackTemplate).subscribe(data => {
            this.messageTemplate = _slackTemplate.messageTemplate;
            this.messageType = _slackTemplate.messageType;
        });
        this.getSlackTemplates();
    }

    sendToSlack(incomingTemplate, issues) {
        let fixed;
        console.log('Before sending:');
        console.log(incomingTemplate);
        console.log(issues);

            if (incomingTemplate.includes('<TIME>')
            || incomingTemplate.includes('<NAME>')
            || incomingTemplate.includes('<DATE>')
            || incomingTemplate.includes('<MACISSUES>')
            || incomingTemplate.includes('<MACCLOUDISSUES>')
            || incomingTemplate.includes('<MPPISSUES>')
            || incomingTemplate.includes('<MEMISSUES>')) {
            fixed = incomingTemplate
            .replace('<TIME>', this.TimeFixer())
            .replace('<NAME>', 'namechange')
            .replace('<DATE>', this.DateFixer());

            incomingTemplate = fixed;
            console.log(fixed);
        } else {
            fixed = incomingTemplate;
            this.customMessageInput = '';
            }

        this.slackTemplateService.sendToSlack(fixed, issues, this.postToChannel);
    }

    IssueArrayToString(arr) {

        let string = '';
        for (let i = 0; i < arr.length; i++) {
            string += arr[i] + '\n';
        }
        return string;
    }

    TimeFixer() {
        const d = new Date();
        let datetext = d.toTimeString();
        datetext = datetext.split(' ')[0];
        return datetext;
    }

    DateFixer() {
        const date = new Date();
        const year = date.getFullYear().toString();
        let month = (date.getMonth() + 1).toString();

        if (month.toString().length < 2) {
            month = '0' + month;
        }

        const day = date.getDate().toString();
        const fullDate = year + '/' + month + '/' + day;

        return fullDate;
    }

    LogJiraRelease() {
        console.log('LogJiraRelease (goIssues):');
        console.log(this.goIssues);

    }


}
/*
export class GoIssue {
    public key: string;
    public inRelease: boolean;
}
*/

            /*
            this.subscriptionGoIssues = this.jiraReleaseService.getJiraReleases().subscribe((goIssues: any) => {
                console.log('go issues (getJiraReleases):');
                console.log(goIssues);
                this.goIssues = goIssues;
              }, (error: any) => {
                if (typeof error['Errors'] != 'undefined') {
                    console.log('error in getJiraReleases');
                    console.log(error['Errors']);
                }
                }); */



                        // Om är ett template

            /*
            if (issues.MAC.length < 1) {
                incomingTemplate = incomingTemplate.replace('*MAC*:', '').replace('<MACISSUES>', '');
            }
            if (issues.MACCLOUD.length < 1) {
                incomingTemplate = incomingTemplate.replace('*MACCloud*:', '').replace('<MACCLOUDISSUES>', '');
            }
            if (issues.MPP.length < 1) {
                incomingTemplate = incomingTemplate.replace('*MPP*:', '').replace('<MPPISSUES>', '');
            }
            if (issues.MEM.length < 1) {
                console.log('mem length < 1');
                incomingTemplate = incomingTemplate.replace('*MEM*:', '').replace('<MEMISSUES>', '');
            }
            else {
                console.log('mem length not < 1');
            } */
        // if (incomingTemplate.hasOwnProperty('messageTemplate')) {