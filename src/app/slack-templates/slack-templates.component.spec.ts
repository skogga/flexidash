import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlackTemplatesComponent } from './slack-templates.component';

describe('SlackTemplatesComponent', () => {
  let component: SlackTemplatesComponent;
  let fixture: ComponentFixture<SlackTemplatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlackTemplatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlackTemplatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
