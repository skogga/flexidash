import { Component, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';
import {HttpClientModule, HttpClient} from '@angular/common/http';

import { SlackTemplatesService } from './slack-templates.service';
import { SlackMessagesService } from './slack-messages.service';
import { JiraReleaseService } from './jira-release.service';
import { JiraPulsService } from './jira-puls.service';
import { JiraCcbService } from './jira-ccb.service';
import { JiraForestService } from './jira-forest.service';
import { SupportCaseService } from './support-cases.service';
import { AlertsService } from './alerts.service';

import fontawesome from '@fortawesome/fontawesome';
import solid from '@fortawesome/fontawesome-free-solid';
import faTwitter from '@fortawesome/fontawesome-free-brands';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [
    SlackTemplatesService,
    SupportCaseService,
    JiraReleaseService,
    SlackMessagesService,
    JiraPulsService,
    AlertsService,
    JiraCcbService,
    JiraForestService
  ]
})
export class AppComponent {
  title = 'app';
}
