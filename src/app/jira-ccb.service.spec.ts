import { TestBed, inject } from '@angular/core/testing';

import { JiraCcbService } from './jira-ccb.service';

describe('JiraCcbService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JiraCcbService]
    });
  });

  it('should be created', inject([JiraCcbService], (service: JiraCcbService) => {
    expect(service).toBeTruthy();
  }));
});
