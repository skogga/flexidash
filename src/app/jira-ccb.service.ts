import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptionsArgs } from '@angular/http';
import 'rxjs/add/operator/map';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import axios from 'axios' // for promise examples

@Injectable()
export class JiraCcbService {

  constructor(private http: Http) { }


  getJiraCCBSpecific(project, id) {

      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      return this.http.get('http://localhost:3000/api/jiraCCB/' + project + '/' + id,
          {headers: headers})
              .map(res => res.json());
  }

  getJiraFixVersion(id) {


    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get('http://localhost:3000/api/jiraFixVersion/' + id,
        {headers: headers})
            .map(res => res.json());
    }


    getJiraFixVersionIssues(id) {

        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.get('http://localhost:3000/api/jiraFixVersionIssues/' + id,
            {headers: headers})
                .map(res => res.json());
    }


  
    




    /* getJiraFixVersionIssues(fixversionId) {

        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.get('http://localhost:3000/api/jiraFixVersion/' + fixversionId,
            {headers: headers})
                .map(res => res.json());
        } */

}

