import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Http, Response, Headers, URLSearchParams, RequestOptions  } from '@angular/http';
import {BrowserModule} from '@angular/platform-browser';
import {JiraPulsService} from '../jira-puls.service';
import {PieChartComponent} from '../pie-chart/pie-chart.component';
import {GaugeChartComponent} from '../gauge-chart/gauge-chart.component';

import {FormControl, FormGroup, FormBuilder, Validators} from '@angular/forms';

export interface IssueFilter {

    Status?: {
        types?: any[]
    };
    Priority?: {
        types?: any[]
    };
    Labels?: {
        types: any[]
    };
}

@Component({
  selector: 'app-jira-puls',
  templateUrl: './jira-puls.component.html',
  styleUrls: ['./jira-puls.component.css']
})


export class JiraPulsComponent implements OnInit {

  constructor(private jiraPulsService: JiraPulsService, private _fb: FormBuilder) {}
    chosenFilters: any[];
    sortingOptions = ['key', 'title', 'type', 'priority', 'status', 'created', 'updated', 'reporter', 'assignee'];
    activeSort: string;

    filterControl = new FormControl();
    filters = [
        {
          name: 'Status',
          types: []
        },
        {
          name: 'Priority',
          types: []
        } /*,
        {
            name: 'Labels',
            types: []
          } */
    ];



    jiraPulsRequestTime: Date;

    macIssues = 0;
    mppIssues = 0;
    memIssues = 0;
    maccIssues = 0;
    changeRequests = 0;
    troubleReports = 0;
    tasks = 0;
    stories = 0;
    totalIssues = 0;

    epicLinksMAC = 0;
    epicLinksMPP = 0;
    epicLinksMEM = 0;
    epicLinksMACC = 0;

    jiraPulses = [];

    printformGroup(model: IssueFilter) {
        // check if model is valid
        // if valid, call API to save customer
        console.log(model);
    }
    debugForm (a, b) {
        console.log(this.filterControl);
        console.log('form');
        console.log(this.chosenFilters);
        console.log(a + '' + '' + b);
    }

    addRemoveFilter($event) {
      console.log($event);
    }

    logSorter() {
        console.log('activeSort: ' + this.activeSort);
    }
    sendPieChartInfo(arr): void {
        // send message to subscribers via observable subject
        this.jiraPulsService.sendPieChartInfo(arr);
    }

    clearPieMessage(): void {
        // clear message
        this.jiraPulsService.clearPieChartInfo();
    }

    sendGaugeChartInfo(arr): void {
        // send message to subscribers via observable subject
        this.jiraPulsService.sendGaugeChartInfo(arr);
      }

    clearGaugeMessage(): void {
        // clear message
        this.jiraPulsService.clearGaugeChartInfo();
    }

    formatDate(d) {

        const year = d.getFullYear().toString();
        let month = (d.getMonth() + 1).toString();
        if (month.length < 2) {
            month = '0' + month;
        }
        let date = d.getDate().toString();
        if (date.length < 2) {
            date = '0' + date;
        }

        let hour = d.getHours().toString();
        if (hour.length < 2) {
        hour = '0' + hour;
        }

        let minutes = d.getMinutes().toString();
        if (minutes.length < 2) {
            minutes = '0' + minutes;
        }

        let seconds = d.getSeconds().toString();
        if (seconds.length < 2) {
            seconds = '0' + seconds;
        }
        const ts = {date: year + '-' + month + '-' + date,
                time: hour + ':' + minutes + ':' + seconds};
        return ts;

    }



    logTypes(issueArray) {

        this.macIssues = 0;
        this.mppIssues = 0;
        this.memIssues = 0;
        this.maccIssues = 0;
        this.changeRequests = 0;
        this.troubleReports = 0;
        this.tasks = 0;
        this.stories = 0;

        this.epicLinksMAC = 0;
        this.epicLinksMPP = 0;
        this.epicLinksMEM = 0;
        this.epicLinksMACC = 0;

        for (let i = 0; i < issueArray.length; i++) {
            for (let j = 0; j < issueArray[i].issues.length; j++) {

                if (issueArray[i].project.toLowerCase() === 'mac') {
                    this.macIssues += 1;
                    if (issueArray[i].issues[j].valueItem) {
                        this.epicLinksMAC += 1;
                    }
                } else if (issueArray[i].project.toLowerCase() === 'mpp') {
                    this.mppIssues += 1;
                    if (issueArray[i].issues[j].valueItem) {
                        this.epicLinksMPP += 1;
                    }
                } else if (issueArray[i].project.toLowerCase() === 'mem') {
                    this.memIssues += 1;
                    if (issueArray[i].issues[j].valueItem) {
                        this.epicLinksMEM += 1;
                    }
                } else if (issueArray[i].project.toLowerCase() === 'macc') {
                    this.maccIssues += 1;
                    if (issueArray[i].issues[j].valueItem) {
                        this.epicLinksMACC += 1;
                    }
                }

                const typeOfIssue = issueArray[i].issues[j].type.toLowerCase();
                if (typeOfIssue === 'task') {
                    this.tasks += 1;
                } else if (typeOfIssue === 'story') {
                    this.stories += 1;
                } else if (typeOfIssue === 'change request') {
                    this.changeRequests += 1;
                } else if (typeOfIssue === 'trouble report') {
                    this.troubleReports += 1;
                }


            }
        }
        this.totalIssues = this.macIssues + this.mppIssues + this.memIssues + this.maccIssues;

        const chartInfo = [
            {
              'name': 'CR',
              'value': this.changeRequests
            },
            {
              'name': 'TR',
              'value': this.troubleReports
            },
            {
              'name': 'Task',
              'value': this.tasks
            },
            {
              'name': 'Story',
              'value': this.stories
            }
        ];

        const chartInfoValueItems = [
            {
              'name': 'MAC',
              'epics': this.epicLinksMAC,
              'total': this.macIssues
            },
            {
              'name': 'MPP',
              'epics': this.epicLinksMPP,
              'total': this.mppIssues
            },
            {
              'name': 'MEM',
              'epics': this.epicLinksMEM,
              'total': this.memIssues
            },
            {
                'name': 'MACC',
                'epics': this.epicLinksMACC,
                'total': this.maccIssues
            }
          ];

        this.sendPieChartInfo(chartInfo);
        this.sendGaugeChartInfo(chartInfoValueItems);
    }

    ngOnInit() {
        this.getJiraPuls();
        // the short way
        /*
        this.myFormGroup = new FormGroup({
            group: new FormGroup({
                type: new FormControl()
            })
        });
        */


    }

    trackByReleaseName(index, rel) {
        return rel.releaseName;
    }

    trackByIssueUpdated(index, iss) {
        return iss.updated.time;
    }

    logStatusTypes (arr) {
        const filterList = {
            statuses: [],
            priorities: [],
            labels: []
        };

        console.log('arr in jira-puls');
        console.log(arr);
        for (let i = 0; i < arr.length; i++) {
            for (let j = 0; j < arr[i].issues.length; j++) {

                if (filterList.statuses.indexOf(arr[i].issues[j].status) < 0) {
                    const status = arr[i].issues[j].status;
                    filterList.statuses.push(status);
                }

                if (filterList.priorities.indexOf(arr[i].issues[j].priority) < 0) {
                    const priority = arr[i].issues[j].priority;
                    filterList.priorities.push(priority);
                }

                for (let k = 0; k < arr[i].issues[j].labels.length; k++) {
                    if (filterList.labels.indexOf(arr[i].issues[j].labels[k]) < 0) {
                        const label = arr[i].issues[j].labels[k];
                        filterList.labels.push(label);
                    }
                }
            }
        }


        const statusIndex = this.filters.findIndex(x => x.name.toLowerCase() === 'status');
        const priorityIndex = this.filters.findIndex(x => x.name.toLowerCase() === 'priority');

        this.filters[statusIndex].types = filterList.statuses;
        this.filters[priorityIndex].types = filterList.priorities;
    }

    isStatus(type) {
        return type.name === 'Status';
    }

    getJiraPuls() {

        this.jiraPulsService.getJiraPuls().subscribe(jiraPulses => {

            const joinedPulses = [];
            for (let i = 0; i < jiraPulses.length; i++) {
                if (jiraPulses[i].hasOwnProperty('MACissues')) {
                    const mac = {
                        project: 'MAC',
                        projectName: 'Maintenance Administration Calculator System',
                        issues: jiraPulses[i].MACissues
                    };
                    joinedPulses.push(mac);
                } else if (jiraPulses[i].hasOwnProperty('MPPissues')) {
                    const mpp = {
                        project: 'MPP',
                        projectName: 'Maintenance Planner Portal',
                        issues: jiraPulses[i].MPPissues
                    };
                    joinedPulses.push(mpp);
                } else if (jiraPulses[i].hasOwnProperty('MEMissues')) {
                    const mem = {
                        project: 'MEM',
                        projectName: 'Maintenance Expert Manager',
                        issues: jiraPulses[i].MEMissues
                    };
                    joinedPulses.push(mem);
                } else if (jiraPulses[i].hasOwnProperty('MACCissues')) {
                    const macc = {
                        project: 'MACC',
                        projectName: 'MACC',
                        issues: jiraPulses[i].MACCissues
                    };
                    joinedPulses.push(macc);
                }
            }


            this.logTypes(joinedPulses);
            this.jiraPulses = joinedPulses;

            this.logStatusTypes(joinedPulses);
            this.jiraPulsRequestTime = new Date();

        },
        (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
            console.log('client side error occurred');
            } else {
            console.log('server side error');
            }
        });
    }



    print(id): void {
        let printContents, popupWin;
        printContents = document.getElementById('print-group-' + id).innerHTML;
        popupWin = window.open('', '_blank', 'scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');

        popupWin.document.open();
        popupWin.document.write(`
        <html>
        <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://static.scania.com/build/global/2.3.0/js/corporate-ui.js"></script>
        <style>
        .printWrapper {
            display: grid;
            grid-template-columns: 40% 20% 20% 40%;
            justify-items: strecth;
            /* horizontally: start, stretch, end, center */
            align-items: start;
            grid-template-rows: auto;
            /* vertically: start, end, center */
        }
        .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12 {
            float: left;
        }
        .col-md-12 {
            width: 100%!important;
        }
        .col-md-11 {
            width: 91.66666667%!important;
        }
        .col-md-10 {
            width: 83.33333333%!important;
        }
        .col-md-9 {
            width: 75%!important;
        }
        .col-md-8 {
            width: 66.66666667%!important;
        }
        .col-md-7 {
            width: 58.33333333%!important;
        }
        .col-md-6 {
            width: 50%!important;
        }
        .col-md-5 {
            width: 41.66666667%!important;
        }
        .col-md-4 {
            width: 33.33333333%!important;
        }
        .col-md-3 {
            width: 25%!important;
        }
        .col-md-2 {
            width: 16.66666667%;!important
        }
        .col-md-1 {
            width: 8.33333333%!important;
        }

        div[class*='Change Request'] {
            border-color: rgb(255, 211, 81)!important;
        }

        div[class*='Trouble Report'] {
            border-color: rgb(208, 68, 55)!important;
        }

        div[class*='Task'] {
            border-color: rgb(75, 173, 232)!important;
        }

        div[class*='Story'] {
            border-color: rgb(99, 186, 60)!important;
        }
        </style>
        `
        );

        const css = document.styleSheets;
        const cssFiles = [];
        for (let i = 0; i < css.length; i++) {
            if (css[i].href) {
                cssFiles.push('<link rel="stylesheet" type="text/css" href="' + css[i].href +  '"/>');
            }
        }

        for (let i = 0; i < cssFiles.length; i++) {
            popupWin.document.write(cssFiles[i]);
        }

        popupWin.document.write(`
        </head>
        <body class="app" onload="window.print(); window.close()">${printContents}</body>
        </html>
        `);
        popupWin.document.close();
    }

}
