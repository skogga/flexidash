import {Component, OnInit, Input, NgModule, OnDestroy , AfterViewInit} from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import * as d3 from 'd3';

import { JiraPulsService } from '../jira-puls.service';
import { JiraReleaseService } from '../jira-release.service';
import { Subscription } from 'rxjs/Subscription';

import { select } from 'd3-selection';
import { arc } from 'd3-shape';


declare var $: any;

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.css']
})
export class PieChartComponent implements OnDestroy {

  // chartInfo: any;
  chartInfoReleasesType: any;
  chartInfoPulsesType: any;
  subscriptionJiraPulsType: Subscription;
  subscriptionJiraReleasesType: Subscription;


  // pie
  showLabels = true;
  explodeSlices = false;
  doughnut = false;
  activeChartIsPuls = false;
  view: any[] = [350, 200];

  // options
  showLegend = true;
  

  crColor = 'rgb(255, 211, 81)';
  trColor = 'rgb(208, 68, 55)';
  taskColor = 'rgb(74, 103, 133)';
  storyColor =  'rgb(20, 137, 44)';
  colorScheme = {
    domain: [
      this.crColor,
      this.trColor,
      this.taskColor,
      this.storyColor
    ]
  };
  data = [
    {
      'name': 'CR',
      'value': 1
    },
    {
      'name': 'TR',
      'value': 1
    },
    {
      'name': 'Task',
      'value': 1
    },
    {
      'name': 'Story',
      'value': 1
    }
  ];

  constructor(private jiraReleaseService: JiraReleaseService, private jiraPulsService: JiraPulsService) {


    this.subscriptionJiraPulsType = this.jiraPulsService.getPieChartInfo().subscribe(chartInfoOne => {
      // puls boards funkar men inte releases?
      // console.log('in pie constructor (puls)');
      this.chartInfoPulsesType = chartInfoOne;
    });
    this.subscriptionJiraReleasesType = this.jiraReleaseService.getPieChartInfo().subscribe(chartInfoTwo => {
      // console.log('in pie constructor (release)');
      this.chartInfoReleasesType = chartInfoTwo;
      this.data = chartInfoTwo;
    });
  }

  ngOnDestroy() {
      // unsubscribe to ensure no memory leaks
      this.subscriptionJiraPulsType.unsubscribe();
      this.subscriptionJiraReleasesType.unsubscribe();
  }


  ngAfterViewInit() {
    const tr = $('g.pie-chart.chart').attr('transform');
    $('g.pie-chart.chart').attr({'transform': tr + ' scale(1, 1)'});
  }


  updateChart(chartInput) {

    if (chartInput === 'releases') {
      this.data = this.chartInfoReleasesType;
    }
    else if (chartInput === 'pulses') {
      this.data = this.chartInfoPulsesType;
    }
  }


  onSelect(event) {
    console.log(event);
  }
}
