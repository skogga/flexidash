import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JiraReleaseComponent } from './jira-release.component';

describe('JiraReleaseComponent', () => {
  let component: JiraReleaseComponent;
  let fixture: ComponentFixture<JiraReleaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JiraReleaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JiraReleaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
