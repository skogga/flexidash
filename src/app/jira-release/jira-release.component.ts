import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Http, Response, Headers, URLSearchParams, RequestOptions  } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { JiraReleaseService } from '../jira-release.service';
import { PieChartComponent } from '../pie-chart/pie-chart.component';
import { GaugeChartComponent } from '../gauge-chart/gauge-chart.component';

@Component({
  moduleId: module.id,
  selector: 'app-jira-release',
  templateUrl: './jira-release.component.html',
  styleUrls: ['./jira-release.component.css']// ,
 // providers:  [ JiraReleaseService ] // denna?
})

export class JiraReleaseComponent implements OnInit {

  jiraReleaseRequestTime: Date;

  macIssues = 0;
  mppIssues = 0;
  memIssues = 0;
  changeRequests = 0;
  troubleReports = 0;
  tasks = 0;
  stories = 0;
  totalIssues = 0;
  epicLinksMAC = 0;
  epicLinksMPP = 0;
  epicLinksMEM = 0;

  businessValueItemsMAC = 0;
  businessValueItemsMPP = 0;
  businessValueItemsMEM = 0;

  jiraReleases = [];

  constructor(public jiraReleaseService: JiraReleaseService) { }

  returnJiraReleases() {
    // console.log('Releases in jira release:');
    // console.log(this.jiraReleases);
    return this.jiraReleases;
  }

  sendPieChartInfo(arr): void {
    // send message to subscribers via observable subject
    this.jiraReleaseService.sendPieChartInfo(arr);
  }

  clearPieMessage(): void {
    // clear message
    this.jiraReleaseService.clearPieChartInfo();
  }

  sendGaugeChartInfo(arr): void {
    // send message to subscribers via observable subject
    this.jiraReleaseService.sendGaugeChartInfo(arr);
  }

  clearGaugeMessage(): void {
      // clear message
      this.jiraReleaseService.clearGaugeChartInfo();
  }


  sendGoIssues(arr): void {
    // send message to subscribers via observable subject
    // console.log('2.) in component sending:');
    // console.log(arr);
    this.jiraReleaseService.sendGoIssues(arr);
  }


  logTypes(issueArray) {

      this.macIssues = 0;
      this.mppIssues = 0;
      this.memIssues = 0;
      this.changeRequests = 0;
      this.troubleReports = 0;
      this.tasks = 0;
      this.stories = 0;

      this.epicLinksMAC = 0;
      this.epicLinksMPP = 0;
      this.epicLinksMEM = 0;

      this.businessValueItemsMAC = 0;
      this.businessValueItemsMPP = 0;
      this.businessValueItemsMEM = 0;

      for (let i = 0; i < issueArray.length; i++) {

          if (issueArray[i].hasOwnProperty('MACissues')) {
            for (let j = 0; j < issueArray[i].MACissues.length; j++) {
                this.macIssues += 1;

                const typeOfIssue = issueArray[i].MACissues[j].type.toLowerCase();
                if (typeOfIssue === 'task') {
                    this.tasks += 1;
                }
                else if (typeOfIssue === 'story') {
                    this.stories += 1;
                }
                else if (typeOfIssue === 'change request') {
                    this.changeRequests += 1;
                }
                else if (typeOfIssue === 'trouble report') {
                    this.troubleReports += 1;
                }

                if (issueArray[i].MACissues[j].valueItem) {
                  this.epicLinksMAC += 1;
                  if (issueArray[i].MACissues[j].valueItem.includes('CBM')) {
                    this.businessValueItemsMAC += 1;
                  }
                }
            }
          }

          if (issueArray[i].hasOwnProperty('MPPissues')) {
            for (let j = 0; j < issueArray[i].MPPissues.length; j++) {
              this.mppIssues += 1;

              const typeOfIssue = issueArray[i].MPPissues[j].type.toLowerCase();
              if (typeOfIssue === 'task') {
                  this.tasks += 1;
              }
              else if (typeOfIssue === 'story') {
                  this.stories += 1;
              }
              else if (typeOfIssue === 'change request') {
                  this.changeRequests += 1;
              }
              else if (typeOfIssue === 'trouble report') {
                  this.troubleReports += 1;
              }
              if (issueArray[i].MPPissues[j].valueItem) {
                this.epicLinksMPP += 1;
                if (issueArray[i].MPPissues[j].valueItem.includes('CBM')) {
                  this.businessValueItemsMPP += 1;
                }
              }
            }
          }
          if (issueArray[i].hasOwnProperty('MEMissues')) {
            for (let j = 0; j < issueArray[i].MEMissues.length; j++) {
              this.memIssues += 1;

              const typeOfIssue = issueArray[i].MEMissues[j].type.toLowerCase();
              if (typeOfIssue === 'task') {
                  this.tasks += 1;
              }
              else if (typeOfIssue === 'story') {
                  this.stories += 1;
              }
              else if (typeOfIssue === 'change request') {
                  this.changeRequests += 1;
              }
              else if (typeOfIssue === 'trouble report') {
                  this.troubleReports += 1;
              }
              if (issueArray[i].MEMissues[j].valueItem) {
                this.epicLinksMEM += 1;
                if (issueArray[i].MEMissues[j].valueItem.includes('CBM')) {
                  this.businessValueItemsMEM += 1;
                }
              }
            }
      }
      }

      const chartInfoTypes = [
          {
            'name': 'CR',
            'value': this.changeRequests
          },
          {
            'name': 'TR',
            'value': this.troubleReports
          },
          {
            'name': 'Task',
            'value': this.tasks
          },
          {
            'name': 'Story',
            'value': this.stories
          }
        ];

        const chartInfoValueItems = [
          {
            'name': 'MAC',
            'epics': this.epicLinksMAC,
            'total': this.macIssues
          },
          {
            'name': 'MPP',
            'epics': this.epicLinksMPP,
            'total': this.mppIssues
          },
          {
            'name': 'MEM',
            'epics': this.epicLinksMEM,
            'total': this.memIssues
          },
          {
              'name': 'MACCloud',
              'epics': 0,
              'total': 0
          }
        ];

      //  console.log('jira-release component (sendPieChartInfo): ');
      //  console.log(chartInfoTypes);
      this.sendPieChartInfo(chartInfoTypes);
      this.sendGaugeChartInfo(chartInfoValueItems);

  }

  ngOnInit() {
    this.getJiraReleases();
  }


  trackByReleaseName(index, rel) {
    return rel.releaseName;
  }

  trackByIssueUpdated(index, iss) {
    return iss.updated.time;
  }

  inList(arr, obj) {
    let inArr = false;
    for (let i = 0; i < arr.length; i++ ) {
      if (obj.releaseName === arr[i].releaseName) {
        inArr = true;

        if (obj.hasOwnProperty('MACissues') && obj.MACissues.length > 0) {
                arr[i].MACissues = obj.MACissues;
                arr[i].MACreleased = obj.MACreleased;
                arr[i].MACreleaseDate = obj.MACreleaseDate;
        } else if (obj.hasOwnProperty('MPPissues') && obj.MPPissues.length > 0) {
                arr[i].MPPissues = obj.MPPissues;
                arr[i].MPPreleased = obj.MPPreleased;
                arr[i].MPPreleaseDate = obj.MPPreleaseDate;
        } else if (obj.hasOwnProperty('MEMissues') && obj.MEMissues.length > 0) {
                arr[i].MEMissues = obj.MEMissues;
                arr[i].MEMreleased = obj.MEMreleased;
                arr[i].MEMreleaseDate = obj.MEMreleaseDate;
        }
      }
    }
    if (!inArr) {
      arr.push(obj);
    }
  }

  arraySort(a, b) {
    if (a.releaseName < b.releaseName) {
      return -1;
    }
    if (a.releaseName > b.releaseName) {
      return 1;
    }
    return 0;
  }



  getJiraReleases() {

    this.jiraReleaseService.getJiraReleases()
        .subscribe(releasesSeparated => {

            const joinedArray = [];

            for (let i = 0; i < releasesSeparated.length; i++) {
              this.inList(joinedArray, releasesSeparated[i]);
            }

            joinedArray.sort(this.arraySort);

            this.logTypes(joinedArray);
            this.jiraReleases = joinedArray;

            this.jiraReleaseRequestTime = new Date();

            this.sendGoIssues(joinedArray);

        },
        (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              console.log('client side error occurred');
            } else {
              console.log('server side error');
            }
        });
  }

}





