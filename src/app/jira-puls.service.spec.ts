import { TestBed, inject } from '@angular/core/testing';

import { JiraPulsService } from './jira-puls.service';

describe('JiraPulsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JiraPulsService]
    });
  });

  it('should be created', inject([JiraPulsService], (service: JiraPulsService) => {
    expect(service).toBeTruthy();
  }));
});
