import { TestBed, inject } from '@angular/core/testing';

import { SlackMessagesService } from './slack-messages.service';

describe('SlackMessagesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SlackMessagesService]
    });
  });

  it('should be created', inject([SlackMessagesService], (service: SlackMessagesService) => {
    expect(service).toBeTruthy();
  }));
});
