
import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class SlackTemplatesService {
    constructor(private http: Http) {
        // console.log('Slack Template service initialized');
    }

    getSlackTemplates() {
        return this.http.get('http://localhost:3000/api/slackTemplates')
        .map(res => res.json());
    }

/*
RD0049523
sesoco3319
localhost:3000
*/


    sendToSlack(template, issues, postToChannel) {
        const mess = template;
        console.log('issues');
        console.log(issues);
        // console.log('mess in service: ' + mess);
        const headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded'); // 'application/x-www-form-urlencoded'
        const data = {
            payload: {
                text: mess,
                attachments: [],
                username: 'FlexBot', /* FlexBot */
                as_user: true,
                icon_emoji: ':postal_horn:' //  // ':robot_face:' 'https://ca.slack-edge.com/T0LUV8H9C-U6P4B48PQ-8292fbda2f21-48'
            }
        };

        if (issues.MAC.length > 0) {


            for (let i = 0; i < issues.MAC.length; i++) {
                data.payload.attachments.push( {
                    'fallback': 'Required plain-text summary of the attachment.',
                    'color': '#93B5C6',
                    'title': issues.MAC[i].key,
                    'title_link': issues.MAC[i].url,
                    'text': issues.MAC[i].title
                });
            }
        }
        if (issues.MACCLOUD.length > 0) {
            for (let i = 0; i < issues.MACCLOUD.length; i++) {
                data.payload.attachments.push( {
                    'fallback': 'Required plain-text summary of the attachment.',
                    'color': '#93B5C6',
                    'title': issues.MACCLOUD[i].key,
                    'title_link': issues.MACCLOUD[i].url,
                    'text': issues.MACCLOUD[i].title
                });
            }
        }
        if (issues.MPP.length > 0) {
            for (let i = 0; i < issues.MPP.length; i++) {
                data.payload.attachments.push( {
                    'fallback': 'Required plain-text summary of the attachment.',
                    'color': '#D7816A',
                    'title': issues.MPP[i].key,
                    'title_link': issues.MPP[i].url,
                    'text': issues.MPP[i].title
                });
            }
        }
        if (issues.MEM.length > 0) {
            for (let i = 0; i < issues.MEM.length; i++) {
                data.payload.attachments.push( {
                    'fallback': 'Required plain-text summary of the attachment.',
                    'color': '#F0CF65',
                    'title': issues.MEM[i].key,
                    'title_link': issues.MEM[i].url,
                    'text': issues.MEM[i].title
                });
            }
        }

        const webHooks = {

            NodeMPP: 'https://hooks.slack.com/services/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',
            NodeMAC: 'https://hooks.slack.com/services/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',
            NodeSupport: 'https://hooks.slack.com/services/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',

            FlexMAC: 'https://hooks.slack.com/services/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',
            FlexMPP: 'https://hooks.slack.com/services/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',
            FlexMEM: 'https://hooks.slack.com/services/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',
            FlexManagement: 'https://hooks.slack.com/services/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',
            FlexSupport: 'https://hooks.slack.com/services/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',
            FlexCleod: 'https://hooks.slack.com/services/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'

        };

        if (postToChannel.MAC) {
            console.log('Posting to mac');
            this.http.post(webHooks.NodeMAC, data.payload, {headers: headers}).subscribe();
            // this.http.post(webHooks.FlexMAC, data.payload, {headers: headers}).subscribe();
        }
        if (postToChannel.Cleod) {
            console.log('Posting to cleoud');
            // this.http.post(webHooks.FlexCleod, data.payload, {headers: headers}).subscribe();
        }
        if (postToChannel.MPP) {
            console.log('Posting to mpp');
            this.http.post(webHooks.NodeMPP, data.payload, {headers: headers}).subscribe();
            // this.http.post(webHooks.FlexMPP, data.payload, {headers: headers}).subscribe();
        }
        if (postToChannel.MEM) {
            console.log('Posting to mem');
            // this.http.post(webHooks.FlexMEM, data.payload, {headers: headers}).subscribe();
        }
        if (postToChannel.Support) {
            console.log('Posting to support');
            this.http.post(webHooks.NodeSupport, data.payload, {headers: headers}).subscribe();
            // this.http.post(webHooks.FlexSupport, data.payload, {headers: headers}).subscribe();
        }
        if (postToChannel.Management) {
            console.log('Posting to management');
          //  this.http.post(webHooks.FlexManagement, data.payload, {headers: headers}).subscribe();
        }
    }

    updateMessageTemplate(slackTemplate) {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.put('http://localhost:3000/api/slackTemplate/' + slackTemplate._id,
            JSON.stringify(slackTemplate),
            {headers: headers})
                .map(res => res.json());
    }

/*
RD0049523
sesoco3319
localhost:3000
*/

}


            /*
            const macFields = [];
            for (let i = 0; i < issues.MAC.length; i++) {
                const field = {
                    title: issues.MAC[i].key,
                    title_link: issues.MAC[i].url,
                    value: issues.MAC[i].title,
                    short: false
                };
                macFields.push(field);
            }

            data.payload.attachments.push( {
                'fallback': 'Required plain-text summary of the attachment.',
                'color': '#51D3FF',
                fields: macFields
            });
            */
