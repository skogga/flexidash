import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptionsArgs } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class SlackMessagesService {

  constructor(private http: Http) {
    // console.log('Slack Messages service initialized');
  }



  getSlackMessages() {
    // console.log('in slack message service');
    return this.http.get('http://localhost:3000/api/slackMessages')
        .map(res => res.json());
  }
}
/*
localhost:3000
RD0049523
sesoco3319
*/
